<!DOCTYPE HTML>
<!--
	Verti by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
        <meta charset="UTF-8"/>
		<title>Petic: Pagina de Inicio</title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->

	</head>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<header>
					<?php
						include("head.php");
					?>
				</header>
				<section id="pagina-contacto" class="container">
					<div id="info">
						<div id="descripcion">
							<h3>Petic, busca tu manada</h3>
							<p>Petic nace con la vocación de mejorar y extender los lazos entre todos aquellos que pertenezcan a la comunidad animal, ya sean dueños o mascotas. Un proyecto pionero donde personas de todo el territorio español pueden relacionarse con todo aquel que comparta su vida con un animal afin al suyo, organizando quedadas, compartiendo información, realizando y resolviendo dudas... En resumen, estás ante una forma única de encontrar tu manada :)</p>
						</div>
						<div id="que-hacemos">
							<h3>&iquest;Qu&eacute; hacemos?</h3>
							<p>Si por algo se caracteriza Petic es por su alto grado de usabilidad, permitiendo al usuario en unos pocos clicks encontrar aquellos animales afines a su mascota, lugar de residencia u otras preferencias personales. El contacto entre ellos es muy sencillo e intuitivo, facilitando así la comunicación.</p>
							<p>Además, nuestra ayuda para que encuentres tu manada no se queda ahí, ya que la web dispone de un sistema de anuncios donde cualquier usuario puede publicar mensajes segmentados en función de los posibles interesados, facilitando así el intercambio de ideas, productos o cualquier información interesante para nosotros y nuestros mejores amigos. Por último, desde la administración proponemos actividades de forma periódica, a las que se podrá sumar cualquier usuario de la página.</p>
							<p>Como no, estamos abiertos a cualquier sugerencia o comentario para mejorar nuestro sistema, ya que somos conscientes de que siempre se puede mejorar, así que, si crees que puedes ayudarnos, por favor envíanos un mensaje en el formulario que observas a tu derecha.</p>
							<p>¡Bienvenidx a tu manada!</p>
							<p> El equipo de Petic.</p>
						</div>
					</div>

					<div id="contacto">
						<div id="redes">
							<h3>Contacta con nosotros</h3>
							<button><i class="fa fa-instagram fa-5x"></i></button>
							<button><i class="fa fa-facebook-square fa-5x"></i></button>
							<button><i class="fa fa-twitter-square fa-5x"></i></button>
						</div>
						<div id="formulario">
							<form id="form">
								<input id="user-email" type="text" name="user-email" placeholder="*Email:">
								<input id="user-name" type="text" name="user-name" placeholder="*Nombre:">
								<input id="user-title" type="text" name="user-title" placeholder="Asunto:">
								<textarea id="user-message" name="user-message" placeholder="*Mensaje:"></textarea>
							</form>
						</div>

					</div>
				</section>
				<footer>
					<?php
						include("footer.php");
					?>
				</footer>
			</div>
		</body>
	</html>