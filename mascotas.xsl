<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
	<html>
	<head>
		<link rel="stylesheet" href="portada.css"/>
		<title>Petic: Mascotas</title>
	</head>
	<body>
		<h1 align="center">Lista de mascotas inscritas en Petic</h1>
		<center>
			
		<table align="center">
			<xsl:apply-templates select="mascotas/mascota"/>
		</table>
		</center>
		<xsl:apply-templates select="mas"/>
	</body></html>
</xsl:template>

<xsl:template match="mascota">
<th align="left">
	<hr/>
	<h3 align="center">
	<xsl:apply-templates select="nombre"/> (<xsl:apply-templates select="sexo"/>)<br/>
	</h3>
</th>
<tr>
	<td align="center">
		 <xsl:apply-templates select="fotografia"/><br/>
		<b>Fecha de Nacimiento: </b><xsl:apply-templates select="fechaNacimiento"/><br/>
	    <b>Descripcion: </b><xsl:apply-templates select="descripcion"/><br/>
	    <b>Raza: </b><xsl:apply-templates select="raza"/><br/>
	    <b>Estado: </b><xsl:apply-templates select="estado"/>
	</td>
	<td align="center">
	</td>
</tr>
</xsl:template>

<xsl:template match="fotografia">
	<xsl:element name="img">
		<xsl:attribute name="src">
			<xsl:value-of select="@ruta"/>
		</xsl:attribute>
		<xsl:attribute name="width">
			200
		</xsl:attribute>
		<xsl:attribute name="height">
			200
		</xsl:attribute>
	</xsl:element>
		<xsl:apply-templates/>

</xsl:template>

</xsl:stylesheet>