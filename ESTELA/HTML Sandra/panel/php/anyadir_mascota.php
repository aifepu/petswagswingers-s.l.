<?
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
if(!isset($_SESSION['administrador']))
{
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php include_once "conexion.php";?>
<?php include_once "imagen_actividad.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>

<header>
  <?
    include ("headeradmin.php");
  ?>
</header>
<body>

    <div class="contenido">
      <form method="post" action="insercion_mascota.php" enctype="multipart/form-data" style="margin-left:1%">
        <div class="form-group">
          <label for="nombre">Nombre</label>
          <input type="text" class="form-control" name="nombre"
                 placeholder="Introduce el nombre de la mascota">
        </div>
        <div class="form-group">
          <label for="fechaNacimiento">Fecha de nacimiento</label>
          <input type="date" class="form-control" name="fechaNacimiento"
                 placeholder="Introduce la fecha de nacimiento">
        </div>
        <div class="form-group">
          <label for="plazastotalesmascotas">Sexo</label>
          <select name="sexo" value="Sexo de la mascota">
            <option value="Macho">Macho</option>
            <option value="Hembra">Hembra</option>
          </select>
        </div>
        <div class="form-group">
          <label for="descripcion">Descripción</label>
          <input type="text" class="form-control" name="descripcion"
                 placeholder="Introduce la descripción de la actividad">
        </div>

        <div class="form-group">
          <label for="estado">Estado de la mascota</label><br>
          <select name="estado" value="Estado de la mascota">
            <option value="Activo">Activo</option>
            <option value="Inactivo">Inactivo</option>
          </select>
        </div>

        <div class="form-group">
          <label>Raza</label><br>
          <?php
            include("dropdownraza.php");
          ?>
        </div>

        <div class="form-group">
            <label>Dueño</label><br>
            <?php
              include("dropdownusuarios.php");
            ?>
        </div>

        <div class="form-group">
          <label for="archivo">Adjuntar un archivo</label>
          <input type="file" name="imagen">
          <p class="help-block">Inserta la imagen de tu mascota</p>
        </div>
<button type="submit" name ="enviar" class="btn btn-default">Enviar</button>
</form>


</body>
</html>
