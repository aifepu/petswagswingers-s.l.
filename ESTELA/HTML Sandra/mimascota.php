<?php
	include ("seguridad.php");
?>
<html>
	<head>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/jquery.bxslider.css" />
		<link rel="stylesheet" href="assets/css/mascotas.css" />


		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
 		<title>Petic: Mi mascota</title>
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>
		<?php

			include_once "php/conexion.php";
			$nick=$_GET["dueno"];
			$nombre=$_GET["nombre"];
			$con = "SELECT email from usuario where nick='".$nick."';";
			$resul = mysql_query($con);
			$fila = mysql_fetch_array($resul);
			$dueno = $fila['email'];

			$consulta = "SELECT m.nombre nombre, m.fechaNacimiento fechaNacimiento, m.fotografia fotografia, m.descripcion descripcion, m.sexo sexo, r.nombre raza, c.nombre ciudad, p.nombre provincia
				FROM mascota m INNER JOIN raza r ON r.codigo=m.codraza
			 	INNER JOIN usuario u ON m.dueno=u.email
			 	LEFT JOIN ciudad c ON u.codciudad=c.codigo
			 	LEFT JOIN provincia p ON c.codigoprovincia=p.codigo
			 	WHERE m.nombre='$nombre' and dueno='$dueno'";
			$resultado = mysql_query($consulta);
		    while ($row = mysql_fetch_array($resultado)) {
		                  unset($nombre, $fechaNacimiento, $fotografia, $descripcion, $sexo, $raza, $ciudad, $provincia);
		                  $nombre = $row['nombre'];
		                  $fechaNacimiento = $row['fechaNacimiento'];
		                  $fotografia = $row['fotografia'];
		                  $descripcion = $row['descripcion'];
		                  $sexo = $row['sexo'];
		                  $raza = $row['raza'];
		                  $ciudad = $row['ciudad'];
		                  $provincia = $row['provincia'];

			}
			$consulta="SELECT calcularedad('$nombre','$dueno') edad";
			$resultado = mysql_query($consulta);
		    while ($row = mysql_fetch_array($resultado)) {
		    	unset($edad);
		    	$edad = $row['edad'];
		    }

		?>
		<div class="container">
			<div id="infomascota-wrapper">
				<div id="cajaiz">
					<div id="imagen">
						<img style="max-width: 200px; max-height: 200px" src="data:image;base64,<?=$fotografia?>">
					</div>
					<div id="cajadatos">
						<p>Edad: <?=$edad.' a&ntilde;os';?> </p>
					<p>Sexo: <?=$sexo;?></p>
					<p>Raza: <?=$raza;?></p>
					<p>Ciudad: <?=$ciudad;?></p>
					<p>Provincia: <?=$provincia;?></p>
					</div>
				</div>
				<div id="cajade">
					<h1><?=$nombre;?></h1>
					<div id="cajasobremi">
					<?=$descripcion;?>
					</div>
					<div id="cajadueno">
					sobre mi dueño
					</div>
				</div>
			</div>
		</div>


		<footer>
			<?php
				include("footer.php");
			?>
		</footer>
		<script src="assets/js/jquery.bxslider.js"></script>
		<script>
		$(document).ready(function(){
		  $('.bxslider').bxSlider();
		});
		</script>
	</body>

</html>
