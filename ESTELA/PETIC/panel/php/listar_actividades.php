<?php
require_once("PHPPaging.lib.php");
$pagina = new PHPPaging;
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
if(!isset($_SESSION['administrador']))
{
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php  include_once "conexion.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>
<header>
  <?php
    include ("headeradmin.php");
  ?>
</header>
<body>
    <div class="contenido">
      <div class="table-responsive">
        <table class="table table-hover">
		        <thead>
			           <tr>
				               <th>Código</th>
                       <th>Plazas para personas</th>
                       <th>Plazas para mascotas</th>
				               <th>Plazas de persona ocupadas</th>
				               <th>Plazas de mascotas ocupadas</th>
                       <th>Precio</th>
                       <th>Inicio inscripción</th>
                       <th>Fin de inscripción</th>
                       <th>Nombre</th>
                       <th>Descripción</th>
                       <th>Ciudad</th>
                       <th>Foto</th>

			           </tr>
		        </thead>
		        <tbody>
                     <form method="post" action="eliminaractividad.php">
                       <input type="number" class="form-control" name="codigo" placeholder="Introduce codigo para eliminar o modificar">
                       <button type="submit" class="btn btn-danger" name="eliminar" style="width:50%;height:50%:float:right;">Eliminar actividad</button>
                       <button  type="submit" class="btn btn-warning" name="modificar" style="width:50%;height:50%;float:left;">Modificar actividad</button>
                   </form>
                   <hr />
                <?php
                $pagina->agregarConsulta("SELECT codigo,plazastotalespersona,plazastotalesmascota,plazasocupadaspersona,plazasocupadasmascota,
                precio,fechaInicio,fechaFin,nombre,descripcion,codCiudad,imagen from actividad");
                $pagina->ejecutar();
                  mysql_query("SET NAMES 'utf8'");


                  while($res=$pagina->fetchResultado()){?>
                    <tr>
                    <td><?php  echo $res['codigo']; ?></td>
                    <td><?php  echo $res['plazastotalespersona']; ?></td>
                    <td><?php  echo $res['plazastotalesmascota'];?></td>
                    <td><?php  echo $res['plazasocupadaspersona']; ?></td>
                    <td><?php  echo $res['plazasocupadasmascota'];?></td>
                    <td><?php  echo $res['precio']; ?></td>
                    <td><?php  echo $res['fechaInicio']; ?></td>
                    <td><?php  echo $res['fechaFin']; ?></td>
                    <td><?php  echo $res['nombre']; ?></td>
                    <td><?php  echo $res['descripcion']; ?></td>
                    <td><?php  echo $res['codCiudad']; ?></td>
                    <td><?php  echo '<img height="30%" width="60%" src="data:image;base64,'.$res['imagen'].' ">'; ?></td>
                    </tr>
                  <?php }mysql_close();?>
            </tbody>
        </table>
        <?php echo 'Paginas '.$pagina->fetchNavegacion(); ?>
      </div>
    </div>

</body>
</html>
