<?php 
	include ("seguridad.php");
	if($_SESSION['nick']!=$_GET["nick"]){
		$miperfil='no';
	}else{
		$miperfil='yes';
	}

?>
<html>
	<head>
		<title>Petic: Perfil</title>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->

		<link rel="stylesheet" href="assets/css/perfil.css" />
 		<link href="assets/bootstrap/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/js/acciones.js"></script>
		
		
	</head>
	<header>
		<?php 
			include("head.php");

		?>
	</header>
	<body>
	<div id="miperfil-wrapper">
		<div class="row"><h3><?php if($miperfil=='yes'){echo 'Mi perfil';}else{}?></h3></div>
			<div id="miperfilcontainer" class="box container">
				<div class="row">
				   <div class="4u 12u(medium)">
				   		<section class="caja feature">
							<img src="images/usericon.png" />
							<div id="caja-miperfil" style="display:<?php  if($miperfil=='yes'){echo '';}else{echo 'none';}?>">

								<button class="btn btn-sample" onclick="window.location.href='editarperfil.php'" id="perfil">Editar perfil</button>
								<button class="btn btn-sample" onclick="darDeBaja('<?= $_SESSION["usuario"];?>'); return false;" id="perfil">Darse de baja</button>
								<br><br><br><br>
								<div class="container">
								<div><h4>Amigos:</h4></div>
								<br><br>
								</div>
								<?php
									include_once "php/conexion.php";
									$usuario = $_SESSION['usuario'];
									$consulta1 = "SELECT * from amistad where usuario1='$usuario' and estado='Aceptada'";
									$consulta2 = "SELECT * from amistad where usuario2='$usuario' and estado='Aceptada'";
									$result1 = mysql_query($consulta1);
									$result2 = mysql_query($consulta2);
									
									echo '<div class="container">';
									
									while($row1 = mysql_fetch_array($result1)){
										
										unset($solicitante1, $connick1,$resulta1,$fila1,$nick1);
										$solicitante1 = $row1['usuario2'];
										$connick1 = "SELECT nick from usuario where email='$solicitante1'";
										$resulta1 = mysql_query($connick1);
										$fila1 = mysql_fetch_array($resulta1);
										$nick1 = $fila1['nick'];
										
										$link="perfil.php?nick=".$nick1;
										echo '<div style="float:left;padding-right:2em">';
										echo		'<a class="btn btn-sample" href="'.$link.'">';
										echo		'<div style="margin-right: 20px;">';
										echo					'<i class="fa fa-user fa-3x" aria-hidden="true"></i>';
										echo				'</div>';
										echo				'<div style="vertical-align: top; margin-top:10%;">';
										echo					'<p>'.$nick1.'</p>';
										echo				'</div>';
										echo		'</a>';
										echo '</div>';
										
										//echo '<a href="../perfil.php?nick='.$nick1.'">'.$nick1.'</a></br>';
									}
									while($row2 = mysql_fetch_array($result2)){
										
										unset($solicitante2, $connick2,$result2,$fila2,$nick2);
										$solicitante2 = $row2['usuario1'];
										$connick2 = "SELECT nick from usuario where email='$solicitante2'";
										$result2 = mysql_query($connick2);
										$fila2 = mysql_fetch_array($result2);
										$nick2 = $fila2['nick'];
										
										//echo '<div class="row"><a href="../perfil.php?nick='.$nick2.'">'.$nick2.'</a></div>';
										$link="perfil.php?nick=".$nick2;
										echo '<div style="float:left">';
										echo		'<a class="btn btn-sample" href="'.$link.'">';
										echo		'<div style="margin-right: 20px;">';
										echo					'<i class="fa fa-user fa-3x" aria-hidden="true"></i>';
										echo				'</div>';
										echo				'<div style="vertical-align: top; margin-top:10%;">';
										echo					'<p>'.$nick2.'</p>';
										echo				'</div>';
										echo		'</a>';
										echo '</div>';
									}
									echo '</div>';
								?>
								
							</div>
							<div class="inner" style="display:<?php  if($miperfil=='yes'){echo 'none';}else{echo '';}?>">
								<?php
									include_once "php/conexion.php";
									
									$nick = $_GET['nick'];
									$insol="php/insertarsolicitudamistad.php?nick=".$nick;
									$bosol="php/eliminarsolicitudamistad.php?nick=".$nick;
									
									$usuario1 = $_SESSION['usuario'];
									
									$connick = "SELECT email from usuario where nick='$nick'";
									$result = mysql_query($connick);
									$fila = mysql_fetch_array($result);
									$usuario2 = $fila['email'];
									$amistad = 'nada';
									
									$con = "SELECT * from amistad where usuario1='$usuario1' and usuario2='$usuario2'";
									$resultado = mysql_query($con);
									$con2 = "SELECT * from amistad where usuario1='$usuario2' and usuario2='$usuario1'";
									$resultado2 = mysql_query($con2);
									
									if($row = mysql_fetch_array($resultado))
									{
										if($row['estado'] == 'Pendiente' || $row['estado'] == 'Rechazada'){
											
											$amistad = 'pendiente';
										}
										elseif($row['estado'] == 'Aceptada')
										{
											$amistad = 'aceptada';
										}
									}
									elseif($row2 = mysql_fetch_array($resultado2))
									{
											if($row2['estado'] == 'Pendiente'){
												
												$amistad = 'paraceptar';
												$rechazarsol = "php/rechazarsolicitudamistad.php?nick=".$nick;
												$aceptarsol = "php/aceptarsolicitudamistad.php?nick=".$nick;
											}
											elseif($row2['estado'] == 'Aceptada')
											{
												$amistad = 'aceptada';
											}
									}
									
									
								?>
								
								<div id="addamigo" style="display:<?php  if($amistad=='nada'){echo '';}else{echo 'none';}?>">
								<button class="btn btn-sample" onclick="window.location.href='<?=$insol;?>'" id="amigos">Enviar solicitud de amistad</button>
								</div>
								<div id="addamigo" style="display:<?php  if($amistad=='pendiente'){echo '';}else{echo 'none';}?>">
								<button class="btn btn-sample" onclick="window.location.href='<?=$bosol;?>'" id="amigos">Cancelar solicitud</button>
								</div>
								<div id="addamigo" style="display:<?php  if($amistad=='aceptada'){echo '';}else{echo 'none';}?>">
								<button class="btn btn-sample" onclick="window.location.href='<?=$bosol;?>'" id="amigos">Eliminar amigo</button>
								</div>
								<div id="addamigo" style="display:<?php  if($amistad=='paraceptar'){echo '';}else{echo 'none';}?>">
								<button class="btn btn-sample" onclick="window.location.href='<?=$aceptarsol;?>'" id="amigos">Aceptar solicitud</button>
								<button class="btn btn-sample" onclick="window.location.href='<?=$rechazarsol;?>'" id="amigos">Rechazar solicitud</button>
								</div>
							</div>
						</section>
					</div>

					 <div class="6u 12u(medium)">
					 	<section class = "caja feature">
					 		<div class="inner">
								<?php 
									include_once "php/conexion.php";
									$nick = $_GET['nick'];
									$consulta = "SELECT email,nick,u.nombre,apellidos, fecha_alta, c.nombre as ciudad, p.nombre as provincia FROM usuario u, ciudad c, provincia p where nick='$nick' and codciudad=c.codigo and codigoprovincia=p.codigo;";
									$resultado = mysql_query($consulta);
									$row = mysql_fetch_array($resultado);
									$email = $row['email'];
									$nick = $row['nick'];
									$nombre = $row['nombre'];
									$apellidos = $row['apellidos'];
									$fechaalta = $row['fecha_alta'];
									$ciudad = $row['ciudad'];
									$provincia = $row['provincia'];
								?> 
								<h2><?php  echo $nick;?></h2>
								
								<?php
									include_once "php/conexion.php";
									
									$usuario1 = $_SESSION['usuario'];
									$nick = $_GET['nick'];
									$sonamigos = 'no';
									
									$connick = "SELECT email from usuario where nick='$nick'";
									$result = mysql_query($connick);
									$row = mysql_fetch_array($result);
									$usuario2 = $row['email'];
									
									if($usuario1==$usuario2){
										$sonamigos='yes';
									}
									
									$consulta1 = "SELECT * from amistad where usuario1='$usuario1' and usuario2='$usuario2' and estado='Aceptada'";
									$consulta2 = "SELECT * from amistad where usuario1='$usuario2' and usuario2='$usuario1' and estado='Aceptada'";
									$result1 = mysql_query($consulta1);
									$result2 = mysql_query($consulta2);
									
									if($row1 = mysql_fetch_array($result1)){
										$sonamigos='yes';
									}
									else if($row2 = mysql_fetch_array($result2)){
										$sonamigos='yes';
									}
								?>
								
								<p>
								<div id="datospersonales" style="display:<?php  if($sonamigos=='yes'){echo '';}else{echo 'none';}?>">
								<label>Email: </label>&nbsp;&nbsp;&nbsp;<?php  echo $email;?></br>
								<label>Nombre: </label>&nbsp;&nbsp;&nbsp;<?php  echo $nombre;?></br>
								<label>Apellidos: </label>&nbsp;&nbsp;&nbsp;<?php  echo $apellidos;?></br>
								<label>Fecha de alta: </label>&nbsp;&nbsp;&nbsp;<?php  echo $fechaalta;?><p>
								</div>
								<div><label>Ciudad: </label>&nbsp;&nbsp;&nbsp;<?php  echo $ciudad;?><p>
								<label>Provincia: </label>&nbsp;&nbsp;&nbsp;<?php  echo $provincia;?><p>
								</div>
							</div>
						</section>
					  </div>

				   </div> <!-- end row -->

				   <div class="container">
				   <div class="row"><h4>Mascotas:</h4></div>
			   		<div id="add" style="display:<?php  if($miperfil=='yes'){echo '';}else{echo 'none';}?>">
						<button class="btn btn-sample" onclick="window.location.href='addmascota.php'" id="perfil">Añadir nueva mascota</button>
					</div>
						<?php 

							include_once "php/conexion.php";

							$usuario = $_GET['nick'];

							$consulta = "SELECT m.nombre, m.fotografia FROM mascota m INNER JOIN  usuario u ON m.dueno=u.email WHERE u.nick='".$nick."';";
							$resultado = mysql_query($consulta);

							$connick = "SELECT nick FROM usuario where email='".$usuario."';";
							$resulnick = mysql_query($connick);
							$fila = mysql_fetch_array($resulnick);
						    $nick = $fila['nick'];
						    echo '<div class="row">';
						    while ($row = mysql_fetch_array($resultado)) {
						    	
						    	$nombre = $row['nombre'];
						    	$foto = $row['fotografia'];
						    	
						    	$link='mascota.php?nombre='.$nombre.'&nick='.$usuario;
						    		
						    		echo 	'<a href="'.$link.'">';
									echo 	'<div class="mismascotas">';
									echo 		'<div class="imagen-mascota">';
									echo 			'<img style="height: 100%; width: 100%" src="data:image;base64,'.$foto.'">';
									echo 		'</div>';
									echo 		'<div class="info-mascota">';
									echo			 '<h4>'.$nombre.'</h4>';
									echo 		'</div>';
									echo 	'</div></a>';


						}
						echo '</div';
						?> 

				   </div>



				</div> <!-- end .main-col -->

			</div>

		</div>






				<footer>
					<?php 
						include("footer.php");
					?>
				</footer>
	</body>

</html>
