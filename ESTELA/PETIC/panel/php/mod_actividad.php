<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php  include_once "conexion.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>

<header>
  <?php
    include ("headeradmin.php");
  ?>
</header>
<body>

    <div class="contenido">
      <form method="post" action="" enctype="multipart/form-data" style="margin-left:1%">
        <div class="form-group">
          <label for="plazastotalespersona">Plazas para personas</label>
          <input required="" type="number" class="form-control" name="plazastotalespersona"
                 placeholder="Introduce cantidad de plazas para personas">
        </div>
        <div class="form-group">
          <label for="plazastotalesmascotas">Plazas para mascotas</label>
          <input required="" type="number" class="form-control" name="plazastotalesmascotas"
                 placeholder="Introduce cantidad de plazas para mascotas">
        </div>
        <div class="form-group">
          <label for="precio">Precio de la actividad</label>
          <input required="" type="number" class="form-control" name="precio"
                 placeholder="Introduce el precio de la actividad">
        </div>
        <div class="form-group">
          <label for="fechaInicio">Fecha de inicio</label>
          <input required="" type="date" class="form-control" name="fechaInicio"
                 placeholder="Introduce la fecha de inicio de inscripción">
        </div>
        <div class="form-group">
          <label for="fechaFin">Fecha fin de inscripción</label>
          <input required="" type="date" class="form-control" name="fechaFin"
                 placeholder="Introduce la fecha de fin de inscripción">
        </div>
        <div class="form-group">
          <label for="nombre">Nombre de la actividad</label>
          <input required="" type="text" class="form-control" name="nombre"
                 placeholder="Introduce el nombre de la actividad">
        </div>
        <div class="form-group">
          <label for="descripcion">Descripción de la actividad</label>
          <input required="" type="text" class="form-control" name="descripcion"
                 placeholder="Introduce la descripción de la actividad">
        </div>
        <div class="form-group">
          <label class="sr-only" for="form-ciudad">Ciudad</label>
          <?php
            include("dropdownciudades.php");
          ?>
          </div>
        </div>

        <div class="form-group">
          <label for="archivo">Adjuntar un archivo</label>
          <input required="" type="file" name="imagen">
          <p class="help-block">Ejemplo de texto de ayuda.</p>
        </div>
<button type="submit" name ="enviar" class="btn btn-default">Enviar</button>
</form>
<?php

include_once "conexion.php";
  if (isset($_POST['enviar'])) {
    $image = addslashes($_FILES['imagen']['tmp_name']);
    $name = addslashes($_FILES['imagen']['name']);
    $image = file_get_contents($image);
    $image = base64_encode($image);

    $plazastotalespersonas = $_POST['plazastotalespersona'];
    $plazastotalesmascotas = $_POST['plazastotalesmascotas'];
    $precio = $_POST['precio'];
    $fechaInicio = $_POST['fechaInicio'];
    $fechaFin = $_POST['fechaFin'];
    $nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];
    $ciudad = $_POST['regciu'];
    $codigo = $_GET['codigo'];

    $query = "UPDATE actividad SET plazastotalespersona='$plazastotalespersonas',plazastotalesmascota='$plazastotalesmascotas',
    precio = '$precio',fechaInicio = '$fechaInicio',fechaFin='$fechaFin',nombre='$nombre',descripcion='$descripcion',imagen = '$image',codCiudad='$ciudad'
    where codigo = '$codigo';";

    mysql_query("SET NAMES 'utf8'");
    $result = mysql_query($query);
    mysql_close($query);
    header("Location: anyadir_actividad.php");


  }

?>

</body>
</html>
