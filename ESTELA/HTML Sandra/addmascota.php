<?php
	include ("seguridad.php");
?>
<html>
	<head>
		<title>Petic: Publicar anuncio</title>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->


 		<link href="assets/bootstrap/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		<script src="assets/js/cargaDatos.js" type="text/javascript"></script>
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>

		<div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<form role="form" enctype="multipart/form-data" action="php/insertarmascota.php" method="post" class="registration-form">
                        	<fieldset>
                        			<h3>Registrar nueva mascota</h3>
		                            <div class="form-bottom">
                        			<div class="box">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-mascotanombre">Nombre</label>
				                        	<input type="datosmascota" name="regmascotanombre" placeholder="Nombre de tu mascota" class="form-mascotanombre form-control" id="regmascotanombre">
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-descripcion">Descripción</label>
				                        	<textarea name="regdescripcion" class="form-descripcion form-control" placeholder="Descripción" id="regdescripcion"></textarea>
				                        </div>
										<div class="form-group">
				                        	<label  for="form-fnac">Fecha de nacimiento</label>
				                        	<input type="date" name="regfnac" class="form-fnac form-control" id="regfnac"></textarea>
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-sexo">Sexo</label>
				                        	<select name="regsexo" class="registerfield" id="regsexo">
												<option value="">Selecciona el sexo de tu mascota</option>
												<option value="Macho">Macho</option>
												<option value="Hembra">Hembra</option>
											</select>
				                        </div>
										  <div class="form-group">
				                        	<label class="sr-only" for="form-categoriaanimal">Categoría Animal</label>
				                        	<?php
												include_once "php/conexion.php";

												$consulta = "SELECT * FROM categoriaanimal";
												$resultado = mysql_query($consulta);

											    echo '<select class="registerfield" name="regcatanimal" id="regcatanimal" onchange="cargaRazas(this.value);return false;">';
												echo'<option value="">Selecciona la categoría animal de tu mascota</option>';
											    while ($row = mysql_fetch_array($resultado)) {

											                  unset($cod, $nombre);
											                  $cod = $row['codigo'];
											                  $nombre = $row['nombre']; 
											                  echo '<option value="'.$cod.'">'.$nombre.'</option>';

											}

											    echo "</select>";
											?> 				                        </div>
										<div class="form-group">
				                        	<label class="sr-only" for="form-raza">Raza</label>
				                        	<div id="selectraza">
					                        	<select class="registerfield" name="regraza" id="regraza">
					                        	<option value="">Selecciona la raza de tu mascota</option>
					                        	</select>
				                        	</div>
				                        </div>
				                        <div class="form-group">
										  <label class="sr-only" for="archivo">Adjuntar un archivo</label>
										  <input type="file" name="imagen">
										  <p class="help-block">Inserta la imagen de tu mascota</p>
										</div>
				                       <button type="submit" name ="enviar" class="btn btn-sample">Enviar</button>
				                    </div>
				                    </div>
			                    </fieldset>
		                    
		                    </form>
		                    
                        </div>
                    </div>
                </div>
                		<footer>
			<?php
				include("footer.php");
			?>
		</footer>
	</body>
</html>