<?php
	include ("seguridad.php");
?>
<html>
	<head>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/jquery.bxslider.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" />
		<link rel="stylesheet" href="assets/css/mascotas.css" />
		<link href="assets/bootstrap/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		        <link rel="stylesheet" href="registroassets/font-awesome/css/font-awesome.min.css">
		

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
 		<title>Petic: Calcular compatibilidad</title>
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>
		<?php

			include_once "php/conexion.php";
			$dueno=$_GET["dueno"];
			$nombre=$_GET["nombre"];
			$consulta = "SELECT m.nombre nombre, m.fechaNacimiento fechaNacimiento, m.fotografia fotografia, m.descripcion descripcion, m.sexo sexo, r.nombre raza, c.nombre ciudad, p.nombre provincia, m.dueno email, u.nombre nombreusuario, u.apellidos apellidos, u.nick nick
				FROM mascota m INNER JOIN raza r ON r.codigo=m.codraza
			 	INNER JOIN usuario u ON m.dueno=u.email
			 	LEFT JOIN ciudad c ON u.codciudad=c.codigo
			 	LEFT JOIN provincia p ON c.codigoprovincia=p.codigo
			 	WHERE m.nombre='$nombre' and u.nick='$dueno'";
			$resultado = mysql_query($consulta);
		    while ($row = mysql_fetch_array($resultado)) {
		                  unset($fechaNacimiento, $fotografia, $descripcion, $sexo, $raza, $ciudad, $provincia, $email, $nick);
		                  $nombre = $row['nombre'];
		                  $fechaNacimiento = $row['fechaNacimiento'];
		                  $fotografia = $row['fotografia']; 
		                  $descripcion = $row['descripcion'];
		                  $sexo = $row['sexo'];
		                  $raza = $row['raza'];
		                  $ciudad = $row['ciudad'];
		                  $provincia = $row['provincia'];
		                  $email = $row['email'];
		                  $nick = $row['nick'];

			}
			$consulta="SELECT calcularedad('$nombre','$email') edad";
			$resultado = mysql_query($consulta);
		    while ($row = mysql_fetch_array($resultado)) {
		    	unset($edad);
		    	$edad = $row['edad'];
		    }
			
		?> 
		<div class="container">
			<div id="infomascota-wrapper">
				<div id="cajaiz">
					<div id="cajaimagen">
						<img height="100%" width="100%" src="data:image;base64,<?=$fotografia?>">
					</div>
				</div>
				<div id="cajade">
					<h1><?=$nombre;?></h1>

						<div>
							</button>
						</div>
							<?php
							$link="php/obtenermascotasmatching.php?nombre=".$nombre."&dueno=".$dueno;
					?>
						<div>
							<button class="btn btn-sample" onclick="window.location.href='<?=$link;?>'" id="matching">Calcular compatibilidad</button>
						</div>

					</div>
				</div>
								   <div class="container">
				   <div class="row"><h4>Mascotas compatibles:</h4></div>
						<?php
							include("php/mostrarmatching.php");
						?>

				   </div>
			</div>
			
		</div>

	
		<footer>
			<?php
				include("footer.php");
			?>
		</footer>
		<script src="assets/js/jquery.bxslider.js"></script>
		<script>
		$(document).ready(function(){
		  $('.bxslider').bxSlider();
		});
		</script>
	</body>

</html>
