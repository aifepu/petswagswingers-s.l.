CREATE VIEW vista_anuncios AS
SELECT c.codigoCategoria AS codigoCategoria, p.codigoProvincia AS codigoProvincia, anuncio.titulo AS titulo, anuncio.contenido AS contenido, usuario.nick AS nick, anuncio.fecha AS fecha
FROM ((anuncio LEFT JOIN anunciovisibleporprovincia p ON p.codigoAnuncio = anuncio.codigo)
LEFT JOIN anunciovisibleporcategoria c ON c.codigoAnuncio = anuncio.codigo)
INNER JOIN usuario
WHERE anuncio.autor = usuario.email AND usuario.estado = 'Activo'
