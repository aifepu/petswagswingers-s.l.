<?php 
	include ("seguridad.php");
?>
<html>
	<head>
		<title>Petic: Busqueda</title>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/busqueda.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<link rel="stylesheet" href="assets/css/mascotas.css" />
		<link rel="stylesheet" href="assets/css/actividades.css" />
 		<link href="assets/bootstrap/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

 		<script type ="text/javascript">

		var paginador1;
		var totalPaginas1;
		var itemsPorPagina1 = 4;
		var numerosPorPagina1 = 3;

		function creaPaginadorAnuncios(totalItems1)
		{
			paginador1 = $("#paginador1");

			totalPaginas1 = Math.ceil(totalItems1/itemsPorPagina1);

			$('<li><a href="#" class="first_link1"><</a></li>').appendTo(paginador1);
			$('<li><a href="#" class="prev_link1">«</a></li>').appendTo(paginador1);

			var pag1 = 0;
			while(totalPaginas1 > pag1)
			{
				$('<li><a href="#" class="page_link1">'+(pag1+1)+'</a></li>').appendTo(paginador1);
				pag1++;
			}


			if(numerosPorPagina1 > 1)
			{
				$(".page_link1").hide();
				$(".page_link1").slice(0,numerosPorPagina1).show();
			}

			$('<li><a href="#" class="next_link1">»</a></li>').appendTo(paginador1);
			$('<li><a href="#" class="last_link1">></a></li>').appendTo(paginador1);

			paginador1.find(".page_link1:first").addClass("active");
			paginador1.find(".page_link1:first").parents("li").addClass("active");

			paginador1.find(".prev_link1").hide();

			paginador1.find("li .page_link1").click(function()
			{
				var irpagina1 =$(this).html().valueOf()-1;
				cargaPaginaAnuncio(irpagina1);
				return false;
			});

			paginador1.find("li .first_link1").click(function()
			{
				var irpagina1 =0;
				cargaPaginaAnuncio(irpagina1);
				return false;
			});

			paginador1.find("li .prev_link1").click(function()
			{
				var irpagina1 =parseInt(paginador1.data("pag")) -1;
				cargaPaginaAnuncio(irpagina1);
				return false;
			});

			paginador1.find("li .next_link1").click(function()
			{
				var irpagina1 =parseInt(paginador1.data("pag")) +1;
				cargaPaginaAnuncio(irpagina1);
				return false;
			});

			paginador1.find("li .last_link1").click(function()
			{
				var irpagina1 =totalPaginas1 -1;
				cargaPaginaAnuncio(irpagina1);
				return false;
			});

			cargaPaginaAnuncio(0);




		}

		function cargaPaginaAnuncio(pagina1)
		{
			var desde1 = pagina1 * itemsPorPagina1;

			$.ajax({
				data:{"param1":"dame","limit":itemsPorPagina1,"offset":desde1, "campo-busqueda":"<?=$_GET['campo-busqueda'] ?>"},
				type:"GET",
				dataType:"json",
				url:"php/busqueda/conexionanuncios.php"
			}).done(function(data,textStatus,jqXHR){

				var lista1 = data.lista;

				$("#anuncios").html("");
				$.each(lista1, function(ind1, elem1){

					var div1 = document.getElementById('anuncios');

					div1.innerHTML = div1.innerHTML +
					'<div class="anuncio container">'+
					'<div class="row">'+
					'<h4>'+elem1.titulo+'</h4>'+
					'</div>'+
					'</br>'+
					'<div class="anunciocontenido">'+elem1.contenido+'</div>'+
					'<div class="row">'+
					'<a href="perfil.php?nick='+elem1.autor+'">'+elem1.autor+'</a><label class ="normal">'+elem1.fecha+'</label>'+
					'</div>'+
					'</div>'+
					'</div>'+'</br>';


				});


			}).fail(function(jqXHR,textStatus,textError){
				alert("Error al realizar la peticion dame".textError);

			});

			if(pagina1 >= 1)
			{
				paginador1.find(".prev_link1").show();

			}
			else
			{
				paginador1.find(".prev_link1").hide();
			}


			if(pagina1 <(totalPaginas1- numerosPorPagina1))
			{
				paginador1.find(".next_link1").show();
			}else
			{
				paginador1.find(".next_link1").hide();
			}

			paginador1.data("pag",pagina1);

			if(numerosPorPagina1>1)
			{
				$(".page_link1").hide();
				if(pagina1 < (totalPaginas1- numerosPorPagina1))
				{
					$(".page_link1").slice(pagina1,numerosPorPagina1 + pagina1).show();
				}
				else{
					if(totalPaginas1 > numerosPorPagina1)
						$(".page_link1").slice(totalPaginas1- numerosPorPagina1).show();
					else
						$(".page_link1").slice(0).show();

				}
			}

			paginador1.children().removeClass("active");
			paginador1.children().eq(pagina1+2).addClass("active");


		}


		$(function()
		{

			$.ajax({

				data:{"param1":"cuantos", "campo-busqueda":"<?=$_GET['campo-busqueda'] ?>"},
				type:"GET",
				dataType:"json",
				url:"php/busqueda/conexionanuncios.php"
			}).done(function(data,textStatus,jqXHR){
				var total1 = data.total;

				creaPaginadorAnuncios(total1);


			}).fail(function(jqXHR,textStatus,textError){
				alert("Error al realizar la peticion cuantos".textError);

			});



		});



		</script>

		<script type ="text/javascript">

		var paginador2;
		var totalPaginas2;
		var itemsPorPagina2 = 8;
		var numerosPorPagina2 = 3;

		function creaPaginadorMascotas(totalItems2)
		{
			paginador2 = $("#paginador2");

			totalPaginas2 = Math.ceil(totalItems2/itemsPorPagina2);

			$('<li><a href="#" class="first_link2"><</a></li>').appendTo(paginador2);
			$('<li><a href="#" class="prev_link2">«</a></li>').appendTo(paginador2);

			var pag2 = 0;
			while(totalPaginas2 > pag2)
			{
				$('<li><a href="#" class="page_link2">'+(pag2+1)+'</a></li>').appendTo(paginador2);
				pag2++;
			}


			if(numerosPorPagina2 > 1)
			{
				$(".page_link2").hide();
				$(".page_link2").slice(0,numerosPorPagina2).show();
			}

			$('<li><a href="#" class="next_link2">»</a></li>').appendTo(paginador2);
			$('<li><a href="#" class="last_link2">></a></li>').appendTo(paginador2);

			paginador2.find(".page_link2:first").addClass("active");
			paginador2.find(".page_link2:first").parents("li").addClass("active");

			paginador2.find(".prev_link2").hide();

			paginador2.find("li .page_link2").click(function()
			{
				var irpagina2 =$(this).html().valueOf()-1;
				cargaPaginaMascota(irpagina2);
				return false;
			});

			paginador2.find("li .first_link2").click(function()
			{
				var irpagina2 =0;
				cargaPaginaMascota(irpagina2);
				return false;
			});

			paginador2.find("li .prev_link2").click(function()
			{
				var irpagina2 =parseInt(paginador2.data("pag")) -1;
				cargaPaginaMascota(irpagina2);
				return false;
			});

			paginador2.find("li .next_link2").click(function()
			{
				var irpagina2 =parseInt(paginador2.data("pag")) +1;
				cargaPaginaMascota(irpagina2);
				return false;
			});

			paginador2.find("li .last_link2").click(function()
			{
				var irpagina2 =totalPaginas2 -1;
				cargaPaginaMascota(irpagina2);
				return false;
			});

			cargaPaginaMascota(0);




		}

		function cargaPaginaMascota(pagina2)
		{
			var desde2 = pagina2 * itemsPorPagina2;

			$.ajax({
				data:{"param1":"dame","limit":itemsPorPagina2,"offset":desde2, "campo-busqueda":"<?=$_GET['campo-busqueda'] ?>"},
				type:"GET",
				dataType:"json",
				url:"php/busqueda/conexionmascotas.php"
			}).done(function(data,textStatus,jqXHR){

				var lista2 = data.lista;

				$("#listado-mascotas").html("");


				$.each(lista2, function(ind2, elem2){

					var div2 = document.getElementById('listado-mascotas');
					var link2='mascota.php?nombre='+elem2.nombre+'&nick='+elem2.nick;
					
					div2.innerHTML = div2.innerHTML +
					'<a href="'+link2+'">'+
					'<div class="caja-mascota">'+
						'<div class="mascota-imagen">'+
							'<img height="100%" width="100%" src="data:image;base64,'+elem2.fotografia+'">'+
						'</div>'+
						'<div class="mascota-info">'+
							'<h3>'+elem2.nombre+'</h3>'+
							elem2.sexo+'<br>'+
							elem2.raza+
					'</div></div></a>';


				});


			}).fail(function(jqXHR,textStatus,textError){
				alert("Error al realizar la peticion dame".textError);

			});

			if(pagina2 >= 1)
			{
				paginador2.find(".prev_link2").show();

			}
			else
			{
				paginador2.find(".prev_link2").hide();
			}


			if(pagina2 <(totalPaginas2- numerosPorPagina2))
			{
				paginador2.find(".next_link2").show();
			}else
			{
				paginador2.find(".next_link2").hide();
			}

			paginador2.data("pag",pagina2);

			if(numerosPorPagina2>1)
			{
				$(".page_link2").hide();
				if(pagina2 < (totalPaginas2- numerosPorPagina2))
				{
					$(".page_link2").slice(pagina2,numerosPorPagina2 + pagina2).show();
				}
				else{
					if(totalPaginas2 > numerosPorPagina2)
						$(".page_link2").slice(totalPaginas2- numerosPorPagina2).show();
					else
						$(".page_link2").slice(0).show();

				}
			}

			paginador2.children().removeClass("active");
			paginador2.children().eq(pagina2+2).addClass("active");


		}


		$(function()
		{

			$.ajax({

				data:{"param1":"cuantos","campo-busqueda":"<?=$_GET['campo-busqueda'] ?>"},
				type:"GET",
				dataType:"json",
				url:"php/busqueda/conexionmascotas.php"
			}).done(function(data,textStatus,jqXHR){
				var total2 = data.total;

				creaPaginadorMascotas(total2);


			}).fail(function(jqXHR,textStatus,textError){
				alert("Error al realizar la peticion cuantos".textError);

			});



		});



		</script>


		<script type ="text/javascript">

		var paginador3;
		var totalPaginas3;
		var itemsPorPagina3 = 10;
		var numerosPorPagina3 = 3;

		function creaPaginadorActividades(totalItems3)
		{
			paginador3 = $("#paginador3");

			totalPaginas3 = Math.ceil(totalItems3/itemsPorPagina3);

			$('<li><a href="#" class="first_link3"><</a></li>').appendTo(paginador3);
			$('<li><a href="#" class="prev_link3">«</a></li>').appendTo(paginador3);

			var pag3 = 0;
			while(totalPaginas3 > pag3)
			{
				$('<li><a href="#" class="page_link3">'+(pag3+1)+'</a></li>').appendTo(paginador3);
				pag3++;
			}


			if(numerosPorPagina3 > 1)
			{
				$(".page_link3").hide();
				$(".page_link3").slice(0,numerosPorPagina3).show();
			}

			$('<li><a href="#" class="next_link3">»</a></li>').appendTo(paginador3);
			$('<li><a href="#" class="last_link3">></a></li>').appendTo(paginador3);

			paginador3.find(".page_link3:first").addClass("active");
			paginador3.find(".page_link3:first").parents("li").addClass("active");

			paginador3.find(".prev_link3").hide();

			paginador3.find("li .page_link3").click(function()
			{
				var irpagina3 =$(this).html().valueOf()-1;
				cargaPagina(irpagina3);
				return false;
			});

			paginador3.find("li .first_link3").click(function()
			{
				var irpagina3 =0;
				cargaPagina(irpagina3);
				return false;
			});

			paginador3.find("li .prev_link3").click(function()
			{
				var irpagina3 =parseInt(paginador3.data("pag")) -1;
				cargaPagina(irpagina3);
				return false;
			});

			paginador3.find("li .next_link3").click(function()
			{
				var irpagina3 =parseInt(paginador3.data("pag")) +1;
				cargaPagina(irpagina3);
				return false;
			});

			paginador3.find("li .last_link3").click(function()
			{
				var irpagina3 =totalPaginas3 -1;
				cargaPagina(irpagina3);
				return false;
			});

			cargaPagina(0);




		}

		function cargaPagina(pagina3)
		{
			var desde3 = pagina3 * itemsPorPagina3;

			$.ajax({
				data:{"param1":"dame","limit":itemsPorPagina3,"offset":desde3, "campo-busqueda":"<?=$_GET['campo-busqueda'] ?>"},
				type:"GET",
				dataType:"json",
				url:"php/busqueda/conexionactividades.php"
			}).done(function(data,textStatus,jqXHR){

				var lista3 = data.lista;

				$("#listado-actividad").html("");

				$.each(lista3, function(ind3, elem3){

					var div3 = document.getElementById('listado-actividad');
					var link3='actividad.php?codigo='+elem3.codigo;
					div3.innerHTML = div3.innerHTML +

					'<a href="'+link3+'">'+
					'<div class="caja-actividad"style="float:left;">'+
						'<div class="mascota-actividad">'+
							'<img height="100%" width="100%" src="data:image;base64,'+elem3.imagen+'">'+
						'</div>'+
						'<div class="actividad-info">'+
							'<h3>'+elem3.nombre+'</h3>'+'<br>'+

					'</div></div></a>';
				});


			}).fail(function(jqXHR,textStatus,textError){
				alert("Error al realizar la peticion dame".textError);

			});

			if(pagina3 >= 1)
			{
				paginador3.find(".prev_link3").show();

			}
			else
			{
				paginador3.find(".prev_link3").hide();
			}


			if(pagina3 <(totalPaginas3- numerosPorPagina3))
			{
				paginador3.find(".next_link3").show();
			}else
			{
				paginador3.find(".next_link3").hide();
			}

			paginador3.data("pag",pagina3);

			if(numerosPorPagina3>1)
			{
				$(".page_link3").hide();
				if(pagina3 < (totalPaginas3- numerosPorPagina3))
				{
					$(".page_link3").slice(pagina3,numerosPorPagina3 + pagina3).show();
				}
				else{
					if(totalPaginas3 > numerosPorPagina3)
						$(".page_link3").slice(totalPaginas3- numerosPorPagina3).show();
					else
						$(".page_link3").slice(0).show();

				}
			}

			paginador3.children().removeClass("active");
			paginador3.children().eq(pagina3+2).addClass("active");


		}


		$(function()
		{

			$.ajax({

				data:{"param1":"cuantos", "campo-busqueda":"<?=$_GET['campo-busqueda'] ?>"},
				type:"GET",
				dataType:"json",
				url:"php/busqueda/conexionactividades.php"
			}).done(function(data,textStatus,jqXHR){
				var total3 = data.total;

				creaPaginadorActividades(total3);


			}).fail(function(jqXHR,textStatus,textError){
				alert("Error al realizar la peticion cuantos".textError);

			});



		});



		</script>
		<script type ="text/javascript">
		function mostrarCaja(caja, nombre){
			if(document.getElementById(nombre).checked){
				$(caja).show();
			}else{
				$(caja).hide();
			}
			//
		}

		</script>
	</head>
	<header>
		<?php 
			include("head.php");
		?>
	</header>
	<body>
	<div class="container">
	<h1>Filtros de b&uacute;squeda</h1>
		<div id="caja-filtros">
			

			
			<div class="checkbox">
			  <label>
			    <input id="input_mascotas" name="input_mascotas" type="checkbox" value="1" onclick="mostrarCaja('#mostrar-mascotas', 'input_mascotas')" checked>
			    Mascotas<br><br>
			  </label>
			</div>
			<br>
			<div class="checkbox">
			  <label>
			    <input id="input_anuncios" name="input_anuncios" type="checkbox" value="1" onclick="mostrarCaja('#mostrar-anuncios', 'input_anuncios')" checked>
			    Anuncios<br><br>
			  </label>	
			</div><br>
			<div class="checkbox">
			  <label>
			    <input id="input_actividades" name="input_actividades" type="checkbox" value="1" onclick="mostrarCaja('#mostrar-actividades', 'input_actividades')" checked>
			    Actividades<br><br>
			  </label>
			</div>
		</div>

		<div id="mostrar-mascotas">
		    <h1>Mascotas</h1>
	        <div id="listado-mascotas">
				
			</div>
			<div class "col-md-12 text-center">
	       		<ul class="pagination" id="paginador2"></ul>
	        </div>
	    </div>

		<div id="mostrar-anuncios">
			<div id="anuncios-wrapper">
				<h1>Anuncios</h1>
				<div id="anuncios" class="anuncios">
				</div>
		        <div class "col-md-12 text-center">
		       		<ul class="pagination" id="paginador1"></ul>
		        </div>
		     </div>
		</div>

		
	    <div id="mostrar-actividades">
	        <h1>Actividades</h1>
			<div id="listado-actividad">

			</div>

			<div class "col-md-12 text-center">
	       		<ul class="pagination" id="paginador3"></ul>
	        </div>
	    </div>

	</div>
	
				<footer>
					<?php 
						include("footer.php");
					?>
				</footer>
	</body>

</html>
