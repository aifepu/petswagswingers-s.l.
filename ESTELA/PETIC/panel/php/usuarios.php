<?php
require_once("PHPPaging.lib.php");
$pagina = new PHPPaging;
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
if(!isset($_SESSION['administrador']))
{
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php  include_once "conexion.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>
<header>
  <?php
    include ("headeradmin.php");
  ?>
</header>
<body>
    <div class="contenido">
      <div class="table-responsive">
        <table class="table table-hover" style="margin-left:1%">
		        <thead>
			           <tr>
				               <th>Email</th>
                       <th>Nick</th>
				               <th>Nombre</th>
                       <th>Apellidos</th>
                       <th>Fecha de alta</th>
                       <th>Estado</th>
                       <th>Ciudad</th>
			           </tr>
		        </thead>
		        <tbody>
              <form method="post" action="eliminarusuario.php">
                <input type="text" class="form-control" name="codigo" placeholder="Introduce email para eliminar o modificar">
                <button type="submit" class="btn btn-danger" name="eliminar" style="width:50%;height:50%:float:right;">Eliminar usuario</button>
                <button type="submit" class="btn btn-warning" name="modificar" style="width:50%;height:50%;float:left;">Modificar usuario</button>
            </form>
                <?php
                $pagina->agregarConsulta("SELECT email,nick,nombre,apellidos,fecha_alta,estado,codciudad from usuario");
                $pagina->ejecutar();
                  mysql_query("SET NAMES 'utf8'");


                  while($res=$pagina->fetchResultado()){?>
                    <tr>
                    <td><?php  echo $res['email']; ?></td>
                    <td><?php  echo $res['nick']; ?></td>
                    <td><?php  echo $res['nombre']; ?></td>
                    <td><?php  echo $res['apellidos']; ?></td>
                    <td><?php  echo $res['fecha_alta']; ?></td>
                    <td><?php  echo $res['estado']; ?></td>
                    <td><?php  echo $res['codciudad']; ?></td>
                    </tr>
                  <?php }?>
            </tbody>
        </table>
        <?php echo 'Paginas '.$pagina->fetchNavegacion(); ?>
      </div>
    </div>


</body>
</html>
