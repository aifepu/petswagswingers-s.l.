
DELIMITER |

CREATE TRIGGER usuario_BI_fecha_alta BEFORE INSERT ON usuario FOR EACH ROW SET NEW.fecha_alta=NOW();
|

CREATE TRIGGER anuncio_BI_fecha_publicacion BEFORE INSERT ON anuncio FOR EACH ROW SET NEW.fecha=NOW();
|


CREATE TRIGGER amistad_BI_fechaamistad BEFORE INSERT ON amistad FOR EACH ROW SET NEW.fecha=NOW();
|


CREATE TRIGGER mensajeatencionalcliente_BI_fechaenvio BEFORE INSERT ON mensajeatencionalcliente FOR EACH ROW SET NEW.fecha=NOW();
|

CREATE TRIGGER estadomascota_AU_usuario AFTER UPDATE ON usuario FOR EACH ROW
BEGIN
	IF OLD.estado!=NEW.estado THEN 
		UPDATE mascota SET estado=NEW.estado WHERE 					NEW.email=mascota.dueno;
	END IF;
END;
|

CREATE TRIGGER reserva_AI_incrementarplazasocupadas AFTER INSERT ON reserva FOR EACH ROW
BEGIN
	DECLARE plazasnuevasper, plazasnuevasmas int;
	DECLARE plazasocuper, plazasocumas int;

	SET plazasnuevasper = NEW.plazasreservadaspersona;
	SET plazasnuevasmas = NEW.plazasreservadasmascota;

	SELECT plazasocupadaspersona INTO plazasocuper FROM 	actividad WHERE codigo=NEW.actividad1;
	SELECT plazasocupadasmascota INTO plazasocumas FROM 	actividad WHERE codigo=NEW.actividad1;

	UPDATE actividad SET 	plazasocupadaspersona=plazasnuevasper+plazasocuper
	WHERE codigo=NEW.actividad1;
	UPDATE actividad SET 	plazasocupadasmascota=plazasnuevasmas+plazasocumas
	WHERE codigo=NEW.actividad1;
END;
|

CREATE TRIGGER reserva_BD_decrementarplazasocupadas BEFORE DELETE ON reserva FOR EACH ROW
BEGIN
	DECLARE plazasnuevasper, plazasnuevasmas int;
	DECLARE plazasocuper, plazasocumas int;

	SET plazasnuevasper = OLD.plazasreservadaspersona;
	SET plazasnuevasmas = OLD.plazasreservadasmascota;

	SELECT plazasocupadaspersona INTO plazasocuper FROM 	actividad WHERE codigo=OLD.actividad1;
	SELECT plazasocupadasmascota INTO plazasocumas FROM 	actividad WHERE codigo=OLD.actividad1;

	UPDATE actividad SET plazasocupadaspersona=plazasocuper-	plazasnuevasper WHERE codigo=OLD.actividad1;
	UPDATE actividad SET plazasocupadasmascota=plazasocumas-	plazasnuevasmas WHERE codigo=OLD.actividad1;
END;
|

CREATE TRIGGER reserva_AU_actualizarplazasocupadas AFTER UPDATE ON reserva FOR EACH ROW
BEGIN
	DECLARE plazasreservadasperA, plazasreservadasperD, 	plazasnuevasper, plazasreservadasmasA, 	plazasreservadasmasD, plazasnuevasmas int;
	DECLARE plazasocuper, plazasocumas int;

	SET plazasreservadasperA = OLD.plazasreservadaspersona;
	SET plazasreservadasperD = NEW.plazasreservadaspersona;

	SET plazasreservadasmasA = OLD.plazasreservadasmascota;
	SET plazasreservadasmasD = NEW.plazasreservadasmascota;

	
	-- Si la operacion da positivo, significa que se han 
	-- incrementado las plazas ocupadas y si da negativo que 
	-- han disminuido 
	SET plazasnuevasper = plazasreservadasperD-	plazasreservadasperA;
	SET plazasnuevasmas = plazasreservadasmasD-	plazasreservadasmasA;

	SELECT plazasocupadaspersona INTO plazasocuper FROM 	actividad WHERE codigo=OLD.actividad1;
	SELECT plazasocupadasmascota INTO plazasocumas FROM 	actividad WHERE codigo=OLD.actividad1;

	UPDATE actividad SET 	plazasocupadaspersona=plazasocuper+plazasnuevasper WHERE 	codigo=OLD.actividad1;
	UPDATE actividad SET 	plazasocupadasmascota=plazasocumas+plazasnuevasmas WHERE 	codigo=OLD.actividad1;
END;
|
