<?php
require_once("PHPPaging.lib.php");
$pagina = new PHPPaging;
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
if(!isset($_SESSION['administrador']))
{
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php  include ("conexion.php");?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>

<header>
  <?php
    include ("headeradmin.php");
  ?>
</header>
<body>

    <div class="contenido">
      <div class="table-responsive">
        <table class="table table-hover" style="margin-left:1%">
		        <thead>
			           <tr>
				               <th>Código</th>
                       <th>Título</th>
				               <th>Contenido</th>
                       <th>Autor</th>
                       <th>Fecha</th>
			           </tr>
		        </thead>
		        <tbody>
              <form method="post" action="eliminaranuncio.php">
                <input type="number" class="form-control" name="codigo" placeholder="Introduce codigo para eliminar o modificar">
                <button type="submit" class="btn btn-danger" name="eliminar" style="width:50%;height:50%:float:right;">Eliminar anuncio</button>
                <button type="submit" class="btn btn-warning" name="modificar" style="width:50%;height:50%;float:left;">Modificar anuncio</button>
            </form>
                <?php
                $pagina->agregarConsulta("select * from anuncio");
                $pagina->ejecutar();
                  mysql_query("SET NAMES 'utf8'");


                  while($res=$pagina->fetchResultado()){?>
                    <tr>
                    <td><?php  echo $res['codigo']; ?></td>
                    <td><?php  echo $res['titulo']; ?></td>
                    <td><?php  echo $res['contenido']; ?></td>
                    <td><?php  echo $res['autor']; ?></td>
                    <td><?php  echo $res['fecha']; ?></td>
                    </tr>
                  <?php }?>
            </tbody>
        </table>
        <?php echo 'Paginas '.$pagina->fetchNavegacion(); ?>
      </div>
    </div>


</body>
</html>
