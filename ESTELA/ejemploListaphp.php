<?
include("../include/inicio_ajax.php");

$accion = $_REQUEST["accion"];
$pk_cliente = $_REQUEST["pk_cliente"];
$filtro_cli_razonsocial = $_REQUEST["filtro_cli_razonsocial"];

switch($accion){
   /*
    *Cargar el listado de clientes con el nombre, poblaci�n, tel�fono y email.
    */
   case "cargalistado":
      ?>
      <div>
         <fieldset>
            <legend>Filtro de b&uacute;squeda</legend>
            <div class="form-row">
               <label for="filtro_cli_razonsocial">Cliente</label>
               <input type="text" name="filtro_cli_razonsocial" id="filtro_cli_razonsocial" value="<?=$filtro_cli_razonsocial?>"/>
            </div>
            <button onclick="cargaListado(); return false;">Buscar</button>
         </fieldset>
      </div>
      <div id="bloque1">
         <button id="button_anyadir" onclick="cargaCliente(0); return false;">+</button>
      </div>
      <?
      $strSQL="SELECT pk_cliente, cli_razonsocial, cli_nombrecomercial, pob_nombre, pro_nombre, cli_telefono1, cli_email
               FROM cliente C 
               LEFT JOIN poblacion P ON C.fk_poblacion=P.pk_poblacion
               LEFT JOIN provincia PR ON C.fk_provincia=PR.pk_provincia
               WHERE C.cli_borrado=0 AND C.fk_usuario='".$_SESSION[PK_USUARIOPANEL]."' ";
               
      if($filtro_cli_razonsocial!="") $strSQL.=" AND ((cli_razonsocial LIKE '%".$filtro_cli_razonsocial."%') OR (cli_nombrecomercial LIKE '%".$filtro_cli_razonsocial."%')) ";
      
      $strSQL.="ORDER BY C.cli_razonsocial ASC ";
      $resRe=bdQuery($strSQL);
      if(bdNumRows($resRe)){
         ?>
            <table>
               <thead>
               <tr>
                  <th><strong>Nombre </strong></th>
                  <th><strong>Poblaci&oacute;n </strong></th>
                  <th><strong>Tel&eacute;fono </strong></th>
                  <th><strong>Email </strong></th>
                </tr>
               </thead>
               <tbody>
         <?
         while($arrRe=bdFetch($resRe)){
   
            $pk_cliente=$arrRe["pk_cliente"];
            //Si tiene nombre comercial aparece este, si no muestra la razon social
            if($arrRe["cli_nombrecomercial"]==NULL){
               $cli_nombre=$arrRe["cli_razonsocial"];
            }else{
               $cli_nombre=$arrRe["cli_nombrecomercial"];
            }

            $cli_poblacion = utf8_encode($arrRe["pob_nombre"]);
            $cli_provincia = utf8_encode($arrRe["pro_nombre"]);

            if($cli_provincia!=NULL){
               $localizacion = $cli_poblacion." (".$cli_provincia.")";
            }else{
               $localizacion = $cli_poblacion;
            }
            
            $cli_telefono1 = $arrRe["cli_telefono1"];
            $cli_email = $arrRe["cli_email"];
            
            ?>
               <tr>
                  <td><?=$cli_nombre?></td>
                  <td><?=$localizacion?></td>
   
                  <td><a href="tel:<?=$cli_telefono1?>"><?=$cli_telefono1?></a></td>
                  <td><a href="mailto:<?=$cli_email?>"><?=$cli_email?></a></td>
                  
                  <td><button onclick="cargaCliente(<?=$pk_cliente?>); return false;"><i class="fa fa-wrench fa-2x"></i></button></td>
                  <td><button onclick="eliminaCliente(<?=$pk_cliente?>); return false;"><i class="fa fa-times  fa-2x"></i></button></td>
                </tr>
            <?
         }
         ?>
            </tbody>
         </table>
         <?
      }else{
      ?>
      <p style="text-align: center;">No existen clientes.</p>
      <?
      }
      bdFree($resRe);

   break;

   
   
   
   /*
    *Mostrar el formulario de cliente
    */
   case "mostrarFormulario":
      /*Condici�n para agregar un nuevo cliente
       *Si es mayor que 0, cargo los datos del cliente*/
       
      if($pk_cliente>0){
         //Cargo datos
         $strSQL="SELECT pk_cliente,
                  cli_razonsocial,
                  cli_nombrecomercial,
                  cli_tipoentidad,
                  cli_cif,
                  cli_direccion,
                  cli_cp,
                  cli_telefono1,
                  cli_telefono2,
                  cli_email,
                  cli_activo,
                  fk_poblacion,
                  fk_provincia,
                  fk_sector,
                  info_numempleados,
                  info_numusuarios,
                  info_departamentos,
                  info_gestionactual,
                  info_conforme,
                  info_necesidad,
                  info_nec_ventas,
                  info_nec_compras,
                  info_nec_stock,
                  info_nec_almacen,
                  info_nec_reporte,
                  info_nec_tarifas,
                  info_app,
                  info_app_otro_valor,
                  info_migracion,
                  info_tipobd,
                  info_comunicacion,
                  info_co_programa,
                  info_co_email,
                  info_contabilidad,
                  info_con_programa,
                  info_con_email,
                  info_documentacion,
                  info_completado
                  FROM Cliente C LEFT JOIN informacioncliente I
                  ON C.pk_cliente=I.fk_cliente
                  WHERE C.pk_cliente='$pk_cliente'";
         $resRe=bdQuery($strSQL);
         if(bdNumRows($resRe)){
            $arrRe=bdFetch($resRe);
            
            foreach($arrRe as $strClave => $strValor){
               $$strClave = $arrRe[$strClave];
            }
            if($cli_nombrecomercial==NULL){
               $cli_Titulo=$cli_razonsocial;
            }else{
               $cli_Titulo=$cli_nombrecomercial;
            }
            
         }
         bdFree($resRe);
      }else{
         $str_Titulo = "Nuevo cliente";
      }
      
      ?>

      <div id="bloque1">
         <?=$str_Titulo;?>
      </div>


      <input class="toggle-box" id="datos_basicos" type="checkbox">
      <label class="label-toggle" for="datos_basicos">Datos cliente</label>
      <div>
        <div class="form-row">
          <label>
             <span>Raz&oacute;n social (*) :</span>
             <input id="cli_razonsocial" name="cli_razonsocial" type="text" value="<?=$cli_razonsocial;?>" >
          </label>
        </div>

        <div class="form-row">
          <label>
             <span>Nombre Comercial:</span>
             <input id="cli_nombrecomercial" name="cli_nombrecomercial" type="text" value="<?=$cli_nombrecomercial;?>" >  
          </label>
        </div>

        <div class="form-row">
          <label>
             <span>Tipo de entidad: </span>
          </label>
          <div class="form-radio-buttons">
             <div>
                <label>
                  <input id="tipo_entidad1" name="cli_tipoentidad" type="radio" value="0" <?if($cli_tipoentidad==0) echo "checked";?>>
                  <span>Aut&oacute;nomo</span>
                </label>
             </div>
             
             <div>
                <label>
                  <input id="tipo_entidad2" name="cli_tipoentidad" type="radio" value="1" <?if($cli_tipoentidad==1) echo "checked";?>>
                  <span>Empresa</span>
                </label>
             </div>
          </div>
        </div>

        <div class="form-row">
          <label>
             <span>CIF/NIF</span>
             <input id="cli_cif" type="text" name="cli_cif"  value="<?=$cli_cif;?>">
          </label>
        </div>

         <div class="form-row">
            <label>
               <span>Direcci&oacute;n:</span>
               <input id="cli_direccion" name="cli_direccion" type="text" value="<?=$cli_direccion;?>">
            </label>
          </div>

          <div class="form-row">
            <label>
               <span>Provincia: </span>
               <select id="fk_provincia" name="fk_provincia"  onchange="cargaPoblaciones(this.value)">
                  <option value="0">Seleccione Provincia...</option>
                  <?
                  $strSQL="SELECT pk_provincia, pro_nombre FROM provincia WHERE pro_borrado='0' ";
                  $resPro=bdQuery($strSQL);
                  if(bdNumRows($resPro)){
                    while($arrPro=bdFetch($resPro)){
                  ?>
                  <option value="<?=$arrPro["pk_provincia"]?>" <?if($arrPro["pk_provincia"]==$fk_provincia) echo "selected";?>>
                    <?=utf8_encode($arrPro["pro_nombre"])?>
                  </option>
                  <?
                    }  
                  }
                  bdFree($resCli);
                  ?>
               </select>
            </label>
          </div>
          
          <div id="caja_poblaciones" class="form-row">
            <label>
               <span>Poblaci&oacute;n:</span>
               <select id="fk_poblacion" name="fk_poblacion">
                  <option value="0">Selecciona provincia...</option>
               </select>
            </label>
          </div>

          <div class="form-row">
            <label>
               <span>C&oacute;digo postal: </span>
               <input id="cli_cp" name="cli_cp" type="text" value="<?=$cli_cp;?>">
            </label>
          </div>

          <div class="form-row">
            <label>
               <span>Tel&eacute;fono (*) : </span>
               <input id="cli_telefono1" name="cli_telefono1" type="tel" value="<?=$cli_telefono1;?>">
            </label>
          </div>

          <div class="form-row">
            <label>
               <span>Tel&eacute;fono auxiliar: </span>
               <input id="cli_telefono2" name="cli_telefono2" type="tel" value="<?=$cli_telefono2;?>">
            </label>
          </div>

          <div class="form-row">
            <label>
               <span>Email (*) : </span>
               <input id="cli_email" name="cli_email" type="email" value="<?=$cli_email;?>">
            </label>
          </div>

          <div class="form-row">
            <label>
               <span>Sector: </span>
               <select id="fk_sector" name="fk_sector">
                  <option value="0">Seleccione Sector...</option>
                  <?
                  $strSQL="SELECT pk_sector, sec_nombre FROM sector WHERE sec_borrado='0' ";
                  $resSec=bdQuery($strSQL);
                  if(bdNumRows($resSec)){
                    while($arrSec=bdFetch($resSec)){
                  ?>
                  <option value="<?=$arrSec["pk_sector"]?>" <?if($arrSec["pk_sector"]==$fk_sector) echo "selected";?>>
                    <?=utf8_encode($arrSec["sec_nombre"])?>
                  </option>
                  <?
                    }  
                  }
                  bdFree($resSec);
                  ?>
               </select>
            </label>
          </div>
        </div>


        <!-- Solamente aparecer� cuando el cliente ya existe. -->

        <?if($pk_cliente>0){
          ?>
          <input class="toggle-box" id="datos_interes" type="checkbox">
          <label class="label-toggle" for="datos_interes">Datos de inter&eacute;s</label>
          <div>
            <fieldset>
               <div class="form-row">
                  <label>
                      <span>Total de empleados: </span>
                      <input id="info_numempleados" name="info_numempleados" type="number" value="<?=$info_numempleados;?>" step="1" min="1" max="999">
                  </label>
               </div>
               <div class="form-row">
                 <label>
                     <span>N&uacute;mero de usuarios: </span>
                     <input id="info_numusuarios" name="info_numusuarios" type="number" value="<?=$info_numusuarios;?>" step="1" min="1" max="999">
                 </label>
               </div>
               <div class="form-row">
                 <label>
                     <span>Departamentos: </span>
                     <textarea id="info_departamentos" name="info_departamentos"><?=$info_departamentos;?></textarea>
                 </label>
               </div>
            </fieldset>

           <fieldset>
             
             <div class="form-row">
                <label>
                    <span>Programa de gesti&oacute;n actual: </span>
                    <input id="info_gestionactual" name="info_gestionactual" type="text" value="<?=$info_gestionactual;?>">
                </label>
             </div>
               
             <div class="form-row">
                <label>
                    <span>&iquest;Conforme con el programa actual? </span>
                </label>
                <div class="form-radio-buttons">
                    <div>
                        <label>
                            <input id="conforme_si" name="info_conforme" type="radio" value="1" <?if($info_conforme==1) echo "checked";?>>
                            <span>S&iacute;</span>
                        </label>
                    </div>
                    
                    <div>
                        <label>
                            <input id="conforme_no" name="info_conforme" type="radio" value="0" <?if($info_conforme==0) echo "checked";?>>
                            <span>No</span>
                        </label>
                    </div>
                </div>
             </div>
           
           </fieldset>
           
           
           <fieldset>           
              <div class="form-row">
                 <label>
                     <span>Necesidad del cliente: </span>
                     <textarea id="info_necesidad" name="info_necesidad" type="text" value="<?=$info_necesidad;?>"></textarea>
                 </label>
              </div>
              <div class="caja_necesidades">
                 <div class="form-row">
                   <ul>
                      <li>
                         <label>
                             <input id="info_nec_ventas" name="info_nec_ventas" type="checkbox" value="1" <?if($info_nec_ventas==1) echo "checked"?>/>
                             <span>Ventas</span>
                         </label>
                      </li>
                      <li>
                         <label>
                             <input id="info_nec_compras" name="info_nec_compras" type="checkbox" value="1" <?if($info_nec_compras==1) echo "checked"?>/>
                             <span>Compras</span>
                          </label>
                      </li>
                      <li>    
                         <label>
                             <input id="info_nec_stock" name="info_nec_stock" type="checkbox" value="1" <?if($info_nec_stock==1) echo "checked"?>/>
                             <span>Stock</span>
                          </label>
                      </li>
                      <li>        
                         <label>
                             <input id="info_nec_almacen" name="info_nec_almacen" type="checkbox" value="1" <?if($info_nec_almacen==1) echo "checked"?>/>
                             <span>Almacenes </span>
                         </label>
                      </li>
                      <li>     
                         <label>
                             <input id="info_nec_reporte" name="info_nec_reporte" type="checkbox" value="1" <?if($info_nec_reporte==1) echo "checked"?>/>
                             <span>Reporte APP</span>
                         </label>
                      </li>
                      <li>          
                         <label>
                             <input id="info_nec_tarifas" name="info_nec_tarifas" type="checkbox" value="1" <?if($info_nec_tarifas==1) echo "checked"?>/>
                             <span>Tarifas</span>
                         </label>
                     </li>
                   </ul> 
                 </div>
              </div>
              <div class="form-row">
                <label>
                    <span>Tipo de app: </span>
                </label>
                <div class="form-radio-buttons">
                    <div>
                        <label>
                            <input id="info_app_integral" name="info_app" type="radio" value="0" <?if($info_app==0) echo "checked";?>>
                            <span>Integral</span>
                        </label>
                    </div>
                    
                     <div>
                        <label>
                            <input id="info_app_parcial" name="info_app" type="radio" value="1" <?if($info_app==1) echo "checked";?>>
                            <span>Parcial</span>
                        </label>
                    </div>
                    <div>
                        <label>
                            <input id="info_app_otro" name="info_app" type="radio" value="2" <?if($info_app==2) echo "checked";?>>
                            <span>Otro</span>
                        </label>
                    </div>
                </div>
                <div id="info_app" <?if($info_app!=2) echo 'style="display:none"';?>>
                   <div class="form-row">
                      <label>
                          <span>Tipo: </span>
                          <textarea id="info_app_otro_valor" name="info_app_otro_valor" type="text"><?=$info_app_otro_valor;?></textarea>
                      </label>
                   </div>
                </div>
              </div>
           </fieldset>
                  
           <fieldset>
           <div class="form-row">
              <label>
                  <span>Migraci&oacute;n: </span>
              </label>
              <div class="form-radio-buttons">
                 
                 <div>
                     <label>
                         <input id="migracion_no" name="info_migracion" type="radio" value="0" <?if($info_migracion==0) echo "checked";?>>
                         <span>No</span>
                     </label>
                 </div>
                 <div>
                     <label>
                         <input id="migracion_parcial" name="info_migracion" type="radio" value="1"  <?if($info_migracion==1) echo "checked";?>>
                         <span>S&oacute;lo maestros</span>
                     </label>
                 </div>
                 <div>
                     <label>
                         <input id="migracion_todo" name="info_migracion" type="radio" value="2"  <?if($info_migracion==2) echo "checked";?>>
                         <span>Todo</span>
                     </label>
                 </div>
                 
              </div>
           </div>
           <div id="campos_migracion" <?if($info_migracion==0) echo 'style="display:none"';?>>
              <div class="form-row">
                 <label>
                     <span>Tipo de base de datos: </span>
                     <input id="info_tipobd" name="info_tipobd" type="text" value="<?=$info_tipobd;?>">
                 </label>
              </div>     
              <div class="form-row">              
                 <label>
                     <span>Comunicaci&oacute;n con programa externo: </span>
                     <input id="info_comunicacion" name="info_comunicacion" type="checkbox" value="1" <?if($info_comunicacion==1) echo "checked"?>>
                 </label>
              </div>
              <div id="campos_comunicacion" <?if($info_comunicacion==0) echo 'style="display:none"';?>>
                 <div class="form-row">
                    <label>
                        <span>Programa utilizado: </span>
                        <input id="info_co_programa" name="info_co_programa" type="text" value="<?=$info_co_programa;?>">
                    </label>
                 </div>
                 <div class="form-row">
                    <label>
                        <span>Email del proveedor: </span>
                        <input id="info_co_email" name="info_co_email" type="email" value="<?=$info_co_email;?>">
                    </label>
                 </div>
              </div>
           </div>
           </fieldset>
           
           
           <fieldset id="ultimo">
              <div class="form-row">
                 <label>
                     <span>Programa de contabilidad: </span>
                 </label>
                 <div class="form-radio-buttons">
                    <div>
                        <label>
                            <input id="contabilidad_si" name="info_contabilidad" type="radio" value="1" <?if($info_contabilidad==1) echo "checked";?>>
                            <span>S&iacute;</span>
                        </label>
                    </div>
                    <div>
                        <label>
                            <input id="contabilidad_no" name="info_contabilidad" type="radio" value="0"  <?if($info_contabilidad==0) echo "checked";?>>
                            <span>No</span>
                        </label>
                    </div>
                 </div>
              </div>
              
              <div id="campos_contabilidad" <?if($info_contabilidad==0) echo 'style="display:none"';?>>
                 <div class="form-row">
                       <label>
                           <span>Programa utilizado: </span>
                           <input id="info_con_programa" name="info_con_programa" type="text" value="<?=$info_con_programa;?>">
                       </label>
                    </div>
                    <div class="form-row">
                       <label>
                           <span>Email del proveedor: </span>
                           <input id="info_con_email" name="info_con_email" type="email" value="<?=$info_con_email;?>">
                       </label>
                    </div>
              </div>
           </fieldset>
           
           
           <div class="form-row">
              <label>
                  <span>Documentaci&oacute;n recogida: </span>
                  <input id="info_documentacion" name="info_documentacion" type="checkbox" value="1" <?if($info_documentacion==1) echo "checked"?>/>
              </label>
           </div>
           <div class="form-row">
              <label>
                  <span>Informaci&oacute;n recogida: </span>
                  <input id="info_completado" name="info_completado" type="checkbox" value="1" <?if($info_completado==1) echo "checked"?>/>
              </label>
           </div>


          </div>
          
        
         
         <!--Se activar�n cuando hagan falta mas campos-->
         <script>
            
            <?
            if($pk_cliente>0){
            ?>
              cargaPoblaciones('<?=$fk_provincia?>','<?=$fk_poblacion?>');
            <?  
            }
            ?>

            $(document).ready(function(){
               $("[name='info_comunicacion']").change(function(){
                  
                  if($("input[name='info_comunicacion']:checked").val()==1){
                    $("#campos_comunicacion").show();
                    $("#info_co_programa").focus();
                  }else{
                     $("#campos_comunicacion").hide();
                     $("#info_co_programa").val("");
                     $("#info_co_email").val("");
                  }
                  
               });
               $("input:radio[name='info_migracion']").change(function(){
                  
                  if($("input[name='info_migracion']:checked").val()>0){
                    $("#campos_migracion").show();
                    $("#info_tipobd").focus();
                  }else{
                     $("#campos_migracion").hide();
                     $("#info_tipobd").val("");
                     //$("#info_comunicacion").val(0);
                  }
                  
               });
               $("input:radio[name='info_contabilidad']").change(function(){
                  
                  if($("input[name='info_contabilidad']:checked").val()>0){
                    $("#campos_contabilidad").show();
                    $("#info_con_programa").focus();
                  }else{
                     $("#campos_contabilidad").hide();
                     $("#iinfo_con_programa").val("");
                     $("#iinfo_con_email").val("");
                  }
                  
               });
               
               $("input:radio[name='info_app']").change(function(){
                  
                  if($("input[name='info_app']:checked").val()>1){
                    $("#info_app").show();
                    $("#info_app_otro_valor").focus();
                  }else{
                     $("#info_app").hide();
                     $("#info_app_otro_valor").val("");
                  }
                  
               });
            });
         </script>

      <?
        }
      ?>
      
      <div id="response" style="display: none;">La reuni&oacute;n se ha a&ntilde;adido con &eacute;xito.</div>
      <div id="valores_incorrectos" style="display: none;">Introduce todos los campos </div>
      <div class="form-row">
         <button onclick="grabaCliente(<?=$pk_cliente?>); return false;">Enviar</button>
         <button onclick="cargaListado(); return false;">Cancelar</button>
      </div>
      <?   
   break;


   case "anyadir":
   case "modificar":
      if($_POST){
         
         $cli_razonsocial = $_POST['cli_razonsocial'];
         $cli_nombrecomercial = $_POST['cli_nombrecomercial'];
         $cli_tipoentidad = $_POST['cli_tipoentidad'];
         $cli_cif= $_POST["cli_cif"];
         $cli_direccion= $_POST["cli_direccion"];
         $cli_cp= $_POST["cli_cp"];
         $cli_telefono1= $_POST["cli_telefono1"];
         $cli_telefono2= $_POST["cli_telefono2"];
         $cli_email= $_POST["cli_email"];
         $cli_activo= $_POST["cli_activo"];
         $fk_poblacion= $_POST["fk_poblacion"];
         $fk_provincia= $_POST["fk_provincia"];
         $fk_sector= $_POST["fk_sector"];
         $info_numempleados=$_POST["info_numempleados"];
         $info_numusuarios=$_POST["info_numusuarios"];
         $info_departamentos=$_POST["info_departamentos"];
         $info_gestionactual=$_POST["info_gestionactual"];
         $info_conforme=$_POST["info_conforme"];   
         $info_necesidad=$_POST["info_necesidad"];
         $info_nec_ventas=$_POST["info_nec_ventas"];
         $info_nec_compras=$_POST["info_nec_compras"];
         $info_nec_stock=$_POST["info_nec_stock"];
         $info_nec_almacen=$_POST["info_nec_almacen"];
         $info_nec_reporte=$_POST["info_nec_reporte"];
         $info_nec_tarifas=$_POST["info_nec_tarifas"];
         $info_app=$_POST["info_app"];
         $info_app_otro_valor=$_POST["info_app_otro_valor"];
         $info_migracion=$_POST["info_migracion"];
         $info_tipobd=$_POST["info_tipobd"];
         $info_comunicacion=$_POST["info_comunicacion"];
         $info_co_programa=$_POST["info_co_programa"];
         $info_co_email=$_POST["info_co_email"];
         $info_contabilidad=$_POST["info_contabilidad"];
         $info_con_programa=$_POST["info_con_programa"];
         $info_con_email=$_POST["info_con_email"];
         $info_documentacion=$_POST["info_documentacion"];
         $info_completado=$_POST["info_completado"];
            
         
         if($accion=="anyadir"){
            $strSQLcliente = "INSERT INTO cliente SET fk_usuario='".$_SESSION[PK_USUARIOPANEL]."', ";
         }else{
            $strSQLcliente = "UPDATE cliente SET ";
         }

          $strSQLcliente .= "cli_razonsocial='$cli_razonsocial',
          cli_nombrecomercial='$cli_nombrecomercial',
          cli_tipoentidad='$cli_tipoentidad',
          cli_cif='$cli_cif',
          cli_direccion='$cli_direccion',
          cli_cp='$cli_cp',
          cli_telefono1='$cli_telefono1',
          cli_telefono2='$cli_telefono2',
          cli_email='$cli_email',
          fk_poblacion='$fk_poblacion',
          fk_provincia='$fk_provincia',
          fk_sector='$fk_sector',
          cli_activo='1' ";


          if($accion=="modificar"){
            $pk_cliente=$_POST["pk_cliente"];
            $strSQLcliente .= "WHERE pk_cliente='$pk_cliente' ";
            bdQuery($strSQLcliente);
          }else{
            bdQuery($strSQLcliente);
            $pk_cliente=bdInsertId();
          }
          
         
         if($accion=="anyadir"){
            $strSQLinfo = "INSERT INTO informacioncliente SET fk_cliente='$pk_cliente', "; 
         }else{
            $strSQLinfo = "UPDATE informacioncliente SET ";
         }

        //a�adir observaciones de la reunion
          $strSQLinfo .= "info_numempleados='$info_numempleados',
            info_numusuarios='$info_numusuarios',
            info_departamentos='$info_departamentos',
            info_gestionactual='$info_gestionactual',
            info_conforme='$info_conforme',
            info_necesidad='$info_necesidad',
            info_nec_ventas='$info_nec_ventas',
            info_nec_compras='$info_nec_compras',
            info_nec_stock='$info_nec_stock',
            info_nec_almacen='$info_nec_almacen',
            info_nec_reporte='$info_nec_reporte',
            info_nec_tarifas='$info_nec_tarifas',
            info_app='$info_app',
            info_app_otro_valor='$info_app_otro_valor',
            info_migracion='$info_migracion',
            info_tipobd='$info_tipobd',
            info_comunicacion='$info_comunicacion',
            info_co_programa='$info_co_programa',
            info_co_email='$info_co_email',
            info_contabilidad='$info_contabilidad',
            info_con_programa='$info_con_programa',
            info_con_email='$info_con_email',
            info_documentacion='$info_documentacion',
            info_completado='$info_completado' ";

        if($accion=="modificar"){
            $strSQLinfo .= "WHERE fk_cliente='$pk_cliente' ";
          }
          bdQuery($strSQLinfo);

         echo "Cliente grabado con &eacute;xito.";
      }      
   break;

    case "eliminar":
      $strSQL="UPDATE cliente SET cli_borrado='1' WHERE pk_cliente='$pk_cliente' ";
      $resRe=bdQuery($strSQL);
      bdFree($resRe);
   break;

}