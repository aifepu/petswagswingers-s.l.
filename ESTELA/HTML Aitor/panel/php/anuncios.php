<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php include_once "conexion.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>

<body>

  <div class="titulo">
    <img src="../img/logo.png" width="25%" height="25%">
  </div>

  <nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>

  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Actividades <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="listar_actividades.php">Listar actividades</a></li>
          <li><a href="anyadir_actividad.php">Añadir actividad</a></li>
        </ul>
      </li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuarios <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="usuarios.php">Listar usuarios</a></li>
        </ul>
      </li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mascotas <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="mascotas.php">Listar mascotas</a></li>
        </ul>
      </li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anuncios <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="anuncios.php">Listar anuncios</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>

    <div class="contenido">
      <div class="table-responsive">
        <table class="table table-hover" style="margin-left:1%">
		        <thead>
			           <tr>
				               <th>Código</th>
                       <th>Título</th>
				               <th>Contenido</th>
                       <th>Autor</th>
                       <th>Fecha</th>
			           </tr>
		        </thead>
		        <tbody>
                <?
                  $consulta="SELECT * from anuncio";
                  $resultado = mysql_query($consulta);

                  while($fila=mysql_fetch_array($resultado)){?>
                    <tr>
                    <td><?php echo $fila['codigo']; ?></td>
                    <td><?php echo $fila['titulo']; ?></td>
                    <td><?php echo $fila['contenido']; ?></td>
                    <td><?php echo $fila['autor']; ?></td>
                    <td><?php echo $fila['fecha']; ?></td>
                    </tr>
                  <?}?>
            </tbody>
        </table>
      </div>
    </div>


</body>
</html>
