<?
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
if(!isset($_SESSION['administrador']))
{
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php include_once "conexion.php";?>
<?php include_once "imagen_actividad.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>

<header>
  <?
    include ("headeradmin.php");
  ?>
</header>
<body>

    <div class="contenido">
      <form method="post" action="insercion_usuario.php" style="margin-left:1%">
        <div class="form-group">
          <label for="email">Email del usuario</label>
          <input type="email" class="form-control" name="email"
                 placeholder="Introduce el email del usuario">
        </div>
        <div class="form-group">
          <label for="nick">Nick del usuario</label>
          <input type="text" class="form-control" name="nick"
                 placeholder="Introduce el nick del usuario">
        </div>
        <div class="form-group">
          <label for="contrasenya">Contrasenya del usuario</label>
          <input type="password" class="form-control" name="contrasenya"
                 placeholder="Introduce la contrasenya">
        </div>
        <div class="form-group">
          <label for="nombre">Nombre del usuario</label>
          <input type="text" class="form-control" name="nombre"
                 placeholder="Introduce el nombre del usuario">
        </div>
        <div class="form-group">
          <label for="apellidos">Apellidos del usuario</label>
          <input type="text" class="form-control" name="apellidos"
                 placeholder="Introduce los apellidos del usuario">
        </div>
        <div class="form-group">
          <select name="estado" value="Estado del usuario">
            <option value="Activo">Activo</option>
            <option value="Inactivo">Inactivo</option>
          </select>
        </div>

        <button type="submit" name ="enviar" class="btn btn-default">Enviar</button>
</form>


</body>
</html>
