<?php 
	include ("seguridad.php");
	if($_SESSION['nick']!=$_GET["nick"]){
		$mimascota='no';
	}else{
		$mimascota='yes';
	}
?>
<html>
	<head>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/jquery.bxslider.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" />
		<link rel="stylesheet" href="assets/css/mascotas.css" />
		<link rel="stylesheet" href="registroassets/font-awesome/css/font-awesome.min.css">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

		<script type="text/javascript" src="assets/js/acciones.js"></script>
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
 		<title>Petic: Mascota</title>
	</head>
	<header>
		<?php 
			include("head.php");
		?>
	</header>
	<body>
		<?php 

			include_once "php/conexion.php";
			$nick=$_GET["nick"];
			$nombre=$_GET["nombre"];
			$consulta = "SELECT m.nombre nombre, m.fechaNacimiento fechaNacimiento, m.fotografia fotografia, m.descripcion descripcion, m.sexo sexo, r.nombre raza, c.nombre ciudad, p.nombre provincia, m.dueno email, u.nombre nombreusuario, u.apellidos apellidos, u.nick nick
				FROM mascota m INNER JOIN raza r ON r.codigo=m.codraza
			 	INNER JOIN usuario u ON m.dueno=u.email
			 	LEFT JOIN ciudad c ON u.codciudad=c.codigo
			 	LEFT JOIN provincia p ON c.codigoprovincia=p.codigo
			 	WHERE m.nombre='$nombre' and u.nick='$nick'";
			$resultado = mysql_query($consulta);
		    while ($row = mysql_fetch_array($resultado)) {
		                  unset($fechaNacimiento, $fotografia, $descripcion, $sexo, $raza, $ciudad, $provincia, $email, $nick);
		                  $nombre = $row['nombre'];
		                  $fechaNacimiento = $row['fechaNacimiento'];
		                  $fotografia = $row['fotografia']; 
		                  $descripcion = $row['descripcion'];
		                  $sexo = $row['sexo'];
		                  $raza = $row['raza'];
		                  $ciudad = $row['ciudad'];
		                  $provincia = $row['provincia'];
		                  $email = $row['email'];
		                  $nick = $row['nick'];

			}
			$consulta="SELECT calcularedad('$nombre','$email') edad";
			$resultado = mysql_query($consulta);
		    while ($row = mysql_fetch_array($resultado)) {
		    	unset($edad);
		    	$edad = $row['edad'];
		    }
			
		?> 
		<div class="container">
			<div id="infomascota-wrapper">
				<div id="cajaiz">
					<div id="cajaimagen">
						<img height="100%" width="100%" src="data:image;base64,<?php  echo $fotografia?>">
					</div>
					<div id="cajadatos">
						<p>Edad: <?php  echo $edad.' a&ntilde;os';?> </p>
					<p>Sexo: <?php  echo $sexo;?></p>
					<p>Raza: <?php  echo $raza;?></p>
					<p>Ciudad: <?php  echo $ciudad;?></p>
					<p>Provincia: <?php  echo $provincia;?></p>
					</div>
				</div>
				<div id="cajade">
					<h1><?php  echo $nombre;?></h1>
					<div id="cajasobremi">
					<?php  echo $descripcion;?>
					</div>
					<div id="cajadueno">
					<?php 
					$link="perfil.php?nick=".$nick;
					?>
						<div>
							<button onclick="window.location.href='<?php  echo $link;?>'" id="perfil">
									<div style="margin-right: 20px;">
										<i class="fa fa-user fa-3x" aria-hidden="true"></i>
									</div>
									<div style="vertical-align: top; margin-top:10%;">
										<p><?php  echo $nick;?></p>
									</div>
							</button>
						</div>
						<?php
						$link2='editarmascota.php?nombre='.$nombre.'&nick='.$nick;
						?>
						<div id="mimascota" style="display:<?php if($mimascota=='yes'){echo '';}else{echo 'none';}?>">
							<button onclick="window.location.href='<?php  echo $link2;?>'" id="perfil"><i class="fa fa-wrench fa-2x" aria-hidden="true"></i></a>
							<button onclick="eliminaMascota('<?=$nick;?>', '<?=$nombre;?>', '<?=$_SESSION["usuario"]?>'); return false;" id="perfil"><i class="fa fa-close fa-2x" aria-hidden="true"></i></a>
								<?php 
									$ma="matching.php?nombre=".$nombre."&nick=".$nick;
								?>
						
							<button class="btn btn-sample" onclick="window.location.href='<?php  echo $ma;?>'" id="matching">Compatibilidad</button>
				
						</div>

						</button>
					</div>
				</div>
			</div>
		</div>

	<!-- quizas -->
		<div id="interes-wrapper">
			<header>
				<h3>Quiz&aacute;s te interesen</h3>
			</header>
			<div class="spotlights">
								<ul class="bxslider">
									<?php 
									

									include_once "php/conexion.php";
									$consulta="SELECT m.nombre nombre, m.fotografia fotografia, u.nick nick 
												FROM mascota m, usuario u 
												where m.dueno=u.email and m.estado='Activo' and
												u.codciudad =
													(SELECT codciudad FROM usuario WHERE email='$usuario') and
												m.dueno!='$usuario' and
												m.nombre!='$nombre' and
												u.nick!='$nick'
									 			ORDER BY rand()";
									$query=mysql_query($consulta);
									if(mysql_num_rows($query)>0){
										?>
										<li>
										<?php 
										$cont16=0;
										$cont4=0;
										while(($fila=mysql_fetch_array($query)) and ($cont16<16)){
											$nombre=$fila["nombre"];
											$nick=$fila["nick"];
											$fotografia=$fila["fotografia"];

											if($cont4==4){
												$cont4=0;
												?>
												</li>
												<li>
												<?php 
											}
											if($cont4<5){
												$link='"mascota.php?nombre='.$nombre.'&nick='.$nick.'"';
												?>
												<div class="caja-mascota" >
													<a href=<?=$link;?>>

														<?php echo $nombre;?>
														<img height="100%" width="100%" src="data:image;base64,<?php echo $fotografia?>">
													</a>
												</div>
												<?php 
												$cont4++;
											}
											$cont16++;
										}
										?>
										</li>
										<?php 
									}else{
										echo "Error: no existen datos";
									}
									?>
								</ul>

							</div>
						</div>

		</div>

		<footer>
			<?php 
				include("footer.php");
			?>
		</footer>
		<script src="assets/js/jquery.bxslider.js"></script>
		<script>
		$(document).ready(function(){
		  $('.bxslider').bxSlider();
		});
		</script>
	</body>

</html>