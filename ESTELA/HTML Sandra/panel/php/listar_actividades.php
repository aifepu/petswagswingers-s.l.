<?
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
if(!isset($_SESSION['administrador']))
{
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php include_once "conexion.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>
<header>
  <?
    include ("headeradmin.php");
  ?>
</header>
<body>
    <div class="contenido">
      <div class="table-responsive">
        <table class="table table-hover">
		        <thead>
			           <tr>
				               <th>Código</th>
                       <th>Plazas para personas</th>
                       <th>Plazas para mascotas</th>
				               <th>Plazas de persona ocupadas</th>
				               <th>Plazas de mascotas ocupadas</th>
                       <th>Precio</th>
                       <th>Inicio inscripción</th>
                       <th>Fin de inscripción</th>
                       <th>Nombre</th>
                       <th>Descripción</th>
                       <th>Ciudad</th>
                       <th>Foto</th>

			           </tr>
		        </thead>
		        <tbody>
                <?
                  $consulta="SELECT codigo,plazastotalespersona,plazastotalesmascota,plazasocupadaspersona,plazasocupadasmascota,
                  precio,fechaInicio,fechaFin,nombre,descripcion,codCiudad,imagen from actividad";
                  $resultado = mysql_query($consulta);

                  while($fila=mysql_fetch_array($resultado)){?>
                    <tr>
                    <td><?php echo $fila['codigo']; ?></td>
                    <td><?php echo $fila['plazastotalespersona']; ?></td>
                    <td><?php echo $fila['plazastotalesmascota'];?></td>
                    <td><?php echo $fila['plazasocupadaspersona']; ?></td>
                    <td><?php echo $fila['plazasocupadasmascota'];?></td>
                    <td><?php echo $fila['precio']; ?></td>
                    <td><?php echo $fila['fechaInicio']; ?></td>
                    <td><?php echo $fila['fechaFin']; ?></td>
                    <td><?php echo $fila['nombre']; ?></td>
                    <td><?php echo $fila['descripcion']; ?></td>
                    <td><?php echo $fila['codCiudad']; ?></td>
                    <td><?php echo '<img height="30%" width="60%" src="data:image;base64,'.$fila['imagen'].' ">'; ?></td>
                    </tr>
                  <?}mysql_close();?>
            </tbody>
        </table>
      </div>
    </div>

</body>
</html>
