<?
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
if(!isset($_SESSION['administrador']))
{
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<?php include_once "conexion.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<header>
  <?
    include ("headeradmin.php");
  ?>
</header>
<body>
    <div class="contenido">
      <div class="table-responsive">
        <table class="table table-hover" style="margin-left:1%">
		        <thead>
			           <tr>
				               <th>Nombre</th>
                       <th>Fecha de nacimiento</th>
				               <th>Raza</th>
                       <th>Dueño</th>
                       <th>Sexo</th>
                       <th>Estado</th>
                       <th>Descripción</th>
                       <th>Fotografía</th>
			           </tr>
		        </thead>
		        <tbody>
                <?
                  $consulta="SELECT * from mascota";
                  $resultado = mysql_query($consulta);

                  while($fila=mysql_fetch_array($resultado)){?>
                    <tr>
                    <td><?php echo $fila['nombre']; ?></td>
                    <td><?php echo $fila['fechaNacimiento']; ?></td>
                    <td><?php echo $fila['codraza']; ?></td>
                    <td><?php echo $fila['dueno']; ?></td>
                    <td><?php echo $fila['sexo']; ?></td>
                    <td><?php echo $fila['estado']; ?></td>
                    <td><?php echo $fila['descripcion']; ?></td>
                    <td><?php echo '<img height="30%" width="60%" src="data:image;base64,'.$fila['fotografia'].' ">'; ?></td>
                    </tr>
                  <?}?>
            </tbody>
        </table>
      </div>
    </div>


</body>
</html>
