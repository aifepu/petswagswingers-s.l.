<?php 
	include ("seguridad.php");
?>
<html>
	<head>
		<title>Petic: Editar mascota</title>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->


 		<link href="assets/bootstrap/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		<script src="assets/js/cargaDatos.js" type="text/javascript"></script>
	</head>
	<header>
		<?php 
			include("head.php");
		?>
	</header>
	<body>
		
	<?php 
			$usuario = $_SESSION['usuario'];
		 	$nick = $_GET['nick'];
		 	$mascota = $_GET['nombre']; 
		 	$link = 'php/actualizarmascota.php?nick='.$nick.'&nombre='.$mascota;
		 	?>

		 <form method="post" action="<?=$link;?>" style="margin-left:1%">
		 
		 <?php  
		 	include_once "php/conexion.php";
		 	
			$consulta = "SELECT * FROM mascota WHERE dueno='$usuario' AND nombre='$mascota'";
			$resultado = mysql_query($consulta);
			$row = mysql_fetch_array($resultado);
			
			$descripcion=$row['descripcion'];
			$sexo= $row['sexo'];
			$raza= $row['codraza'];
		 ?>
		 
		 <div id="publicaranuncios-wrapper" style="display:<?php if($nick==$_SESSION['nick']){echo '';}else{echo 'none';}?>">
			<div class="cabeceraanuncios"><h3>Editar mascota</h3></div>
			<div class="anuncio container" id="formulariopublicar">
				<div class="form-group">
				  <label>Nombre:</label>
				  <?php  echo '<input id="regnombre" type="text" class="form-control" name="regnombre"
						 placeholder="'.$mascota.'">' ?>
				</div>
				<div class="form-group">
				  <label>Descripcion:</label>
				  <?php  echo '<textarea name="regdescripcion" class="form-descripcion form-control" 
				  		placeholder="'.$descripcion.'" id="regdescripcion"></textarea>';
				 ?>
						  
				</div>
				          <div class="form-group">
				                        	<label  for="form-sexo">Sexo</label>
				                        	<select name="regsexo" class="registerfield" id="regsexo">
												<option value="">Selecciona el nuevo sexo de tu mascota (si desea cambiarlo)</option>
												<option value="Macho">Macho</option>
												<option value="Hembra">Hembra</option>
											</select>
				                        </div>
			<div class="form-group">
				                        	<label  for="form-categoriaanimal">Selecciona la categor�a animal de tu mascota:</label>
				                        	<?php 
												include_once "php/conexion.php";

												$consulta = "SELECT * FROM categoriaanimal";
												$resultado = mysql_query($consulta);

											    echo '<select class="registerfield" name="regcatanimal" id="regcatanimal" onchange="cargaRazas(this.value);return false;">';
												echo'<option value="">Selecciona la categor�a animal de tu mascota</option>';
											    while ($row = mysql_fetch_array($resultado)) {

											                  unset($cod, $nombre);
											                  $cod = $row['codigo'];
											                  $nombre = $row['nombre']; 
											                  echo '<option value="'.$cod.'">'.$nombre.'</option>';

											}

											    echo "</select>";
											?> 				                        </div>
										<div class="form-group">
				                        	<label  for="form-raza">Selecciona la raza de tu mascota (si desea cambiarla):</label>
				                        	<div id="selectraza">
					                        	<select class="registerfield" name="regraza" id="regraza">
					                        	<option value="">Selecciona la raza de tu mascota</option>
					                        	</select>
				                        	</div>
				                        </div>    
				<button type="submit" name ="enviar" class="btn btn-sample">Guardar cambios</button>
			</div>
		 
        </form>
		<footer>
			<?php 
				include("footer.php");
			?>
		</footer>
	</body>
</html>