DELIMITER |

CREATE FUNCTION comprobarultimamascota(elemail varchar(50)) RETURNS BOOL


BEGIN

	DECLARE cuantos tinyint;
	DECLARE ultima BOOL DEFAULT 0;

SELECT count(*) INTO cuantos FROM mascota where mascota.dueno=elemail;

IF cuantos<2 THEN SET ultima=1;
	END IF;
RETURN ultima;
END;
|

CREATE FUNCTION calcularedad(elnombre varchar(30),eldueno varchar(50)) RETURNS int

BEGIN
DECLARE edad, mes int;


SELECT YEAR(CURDATE())-YEAR(mascota.fechaNacimiento)+IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(mascota.fechaNacimiento,'%m-%d'), 0, -1) INTO edad FROM mascota WHERE nombre=elnombre AND dueno=eldueno;

SELECT MONTH(fechaNacimiento) INTO mes FROM mascota WHERE nombre=elnombre AND dueno=eldueno;

IF edad<1 THEN SET edad=MONTH(CURDATE())- mes;
END IF;

RETURN edad;
END;
|

CREATE FUNCTION plazaslibrespersona(laactividad tinyint unsigned) RETURNS tinyint unsigned

BEGIN
	DECLARE auxplazaslibres, auxplazastotales, 	auxplazasocupadas tinyint unsigned;

	SELECT plazastotalespersona INTO auxplazastotales FROM 	actividad 	WHERE codigo=laactividad;

	SELECT plazasocupadaspersona INTO auxplazasocupadas FROM 	actividad WHERE codigo=laactividad;

SET auxplazaslibres = auxplazastotales - auxplazasocupadas;

RETURN auxplazaslibres;
END;
|

CREATE FUNCTION plazaslibresmascota(laactividad tinyint unsigned) RETURNS tinyint unsigned

BEGIN
	DECLARE auxplazaslibres, auxplazastotales, 	auxplazasocupadas tinyint unsigned;

	SELECT plazastotalesmascota INTO auxplazastotales FROM 	actividad 	WHERE codigo=laactividad;

	SELECT plazasocupadasmascota INTO auxplazasocupadas FROM 	actividad WHERE codigo=laactividad;

SET auxplazaslibres = auxplazastotales - auxplazasocupadas;

RETURN auxplazaslibres;
END;
|





