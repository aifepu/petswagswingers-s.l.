<!DOCTYPE html>
<html lang="en">

    <head>
        				                        	 <!-- <textarea name="regpass" placeholder="Contraseña" 
				                        				class="form-about-yourself form-control" id="Textarea1"></textarea> -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Registro en Petic</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="registroassets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="registroassets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="registroassets/css/form-elements.css">
        <link rel="stylesheet" href="registroassets/css/style.css">
        <script src="assets/js/cargaDatos.js" type="text/javascript"></script>

        <!-- <script> window.onload = function (){$(".errormessage").hide();}</script> -->

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

 

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	
                        	<form role="form" enctype="multipart/form-data" action="php/insertarregistro.php" method="post" class="registration-form">
                        		
                        		<fieldset>
		                        	<div class="form-top">
		                        		<div class="form-top-left">
		                        			<h3>Registro. Paso 1 / 3</h3>
		                            		<p>Tus datos de usuario:</p>
		                        		</div>
		                        		<div class="form-top-right">
		                        			<div class="logo"></div>
		                        		</div>
		                            </div>
		                            <div class="form-bottom">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-email">Email</label>
				                        	<input type="email" name="regemail" placeholder="Email" class="form-email form-control" id="regemail">
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-nick">Nombre de usuario</label>
				                        	<input type="text" name="regnick" placeholder="Nombre de usuario" class="form-nick form-control" id="regnick">
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-pass">Contraseña</label>
				                        	<input type="password" name="regpass" placeholder="Contraseña" class="form-pass form-control" id="regpass" maxlength="20" minlength="6">
				                        </div>
                                        <div class="form-group">
				                        	<label class="sr-only" for="form-passre">Repita la Contraseña</label>
				                        	<input type="password" name="regpassre" placeholder="Repita la Contraseña" class="form-passre form-control" id="regpassre" maxlength="20" minlength="6">
				                        </div>

                                        <div class="errormessage">
                                            <label id="mensajeerror"></label>
                                        </div>
				                        <button type="button" class="btn btn-next">Siguiente</button>
				                    </div>
			                    </fieldset>
			                    
			                    <fieldset>
		                        	<div class="form-top">
		                        		<div class="form-top-left">
		                        			<h3>Registro. Paso 2 / 3</h3>
		                            		<p>Datos personales:</p>
		                        		</div>
		                        		<div class="form-top-right">
		                        			<div class="logo"></div>
		                        		</div>
		                            </div>
		                            <div class="form-bottom">
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-nombre">Nombre</label>
				                        	<input type="dato" name="regnombre" placeholder="Nombre" class="form-nombre form-control" id="regnombre">
				                        </div>
				                        <div class="form-group">
				                    		<label class="sr-only" for="form-apellidos">Apellidos</label>
				                        	<input type="dato" name="regape" placeholder="Apellidos" class="form-apellidos form-control" id="regape">
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-provincia">Provincia</label>
				                        	<?php 
						                        include_once "php/conexion.php";

												$consulta = "SELECT * FROM provincia";
												$resultado = mysql_query($consulta);

											    echo '<select class="registerfield" name="regpro" id="regpro" onchange="cargaCiudades(this.value); return false;">';
												  echo'<option value="">Selecciona tu provincia</option>';
											    while ($row = mysql_fetch_array($resultado)) {

											                  unset($cod, $nombre);
											                  $cod = $row['codigo'];
											                  $nombre = $row['nombre']; 
											                  echo '<option value="'.$cod.'">'.$nombre.'</option>';

											}

											    echo "</select>";
											?> 
				                        </div>
										<div class="form-group">
				                        	<label class="sr-only" for="form-ciudad">Provincia</label>
				                        	<div id="selectciudad">
					                        	<select class="registerfield" name="regciu" id="regciu">
					                        	<option value="">Selecciona tu ciudad</option>
					                        	</select>
				                        	</div>
				                        </div>
				                        <button type="button" class="btn btn-previous">Atrás</button>
				                        <button type="button" class="btn btn-next">Siguiente</button>
				                    </div>
			                    </fieldset>
			                    
			                    <fieldset>
		                        	<div class="form-top">
		                        		<div class="form-top-left">
		                        			<h3>Registro. Paso 3 / 3</h3>
		                            		<p>Registra a tu primera mascota</p>
		                        		</div>
		                        		<div class="form-top-right">
		                        			<div class="logo"></div>
		                        		</div>
		                            </div>
		                            <div class="form-bottom">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-mascotanombre">Nombre</label>
				                        	<input type="datosmascota" name="regmascotanombre" placeholder="Nombre de tu mascota" class="form-mascotanombre form-control" id="regmascotanombre">
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-descripcion">Descripción</label>
				                        	<textarea name="regdescripcion" class="form-descripcion form-control" placeholder="Descripción" id="regdescripcion"></textarea>
				                        </div>
										<div class="form-group">
				                        	<label  for="form-fnac">Fecha de nacimiento</label>
				                        	<input type="date" name="regfnac" class="form-fnac form-control" id="regfnac"></textarea>
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-sexo">Sexo</label>
				                        	<select name="regsexo" class="registerfield" id="regsexo">
												<option value="">Selecciona el sexo de tu mascota</option>
												<option value="Macho">Macho</option>
												<option value="Hembra">Hembra</option>
											</select>
				                        </div>
										  <div class="form-group">
				                        	<label class="sr-only" for="form-categoriaanimal">Categoría Animal</label>
				                        	<?php 
												include_once "php/conexion.php";

												$consulta = "SELECT * FROM categoriaanimal";
												$resultado = mysql_query($consulta);

											    echo '<select class="registerfield" name="regcatanimal" id="regcatanimal" onchange="cargaRazas(this.value);return false;">';
												echo'<option value="">Selecciona la categoría animal de tu mascota</option>';
											    while ($row = mysql_fetch_array($resultado)) {

											                  unset($cod, $nombre);
											                  $cod = $row['codigo'];
											                  $nombre = $row['nombre']; 
											                  echo '<option value="'.$cod.'">'.$nombre.'</option>';

											}

											    echo "</select>";
											?> 				                        </div>
										<div class="form-group">
				                        	<label class="sr-only" for="form-raza">Raza</label>
				                        	<div id="selectraza">
					                        	<select class="registerfield" name="regraza" id="regraza">
					                        	<option value="">Selecciona la raza de tu mascota</option>
					                        	</select>
				                        	</div>
				                        </div>
				                        <div class="form-group">
										  <label class="sr-only" for="archivo">Adjuntar un archivo</label>
										  <input type="file" name="imagen">
										  <p class="help-block">Inserta la imagen de tu mascota</p>
										</div>
				                        <button type="button" class="btn btn-previous">Atrás</button>
				                        <button type="submit" class="btn">¡Registrarme!</button>
				                    </div>
			                    </fieldset>
		                    
		                    </form>
		                    
                        </div>
                    </div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="registroassets/js/jquery-1.11.1.min.js"></script>
        <script src="registroassets/bootstrap/js/bootstrap.min.js"></script>
        <script src="registroassets/js/jquery.backstretch.min.js"></script>
        <script src="registroassets/js/retina-1.1.0.min.js"></script>
        <script src="registroassets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="registroassets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>