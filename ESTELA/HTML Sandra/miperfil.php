<?php
	include ("seguridad.php");
?>
<html>
	<head>
		<title>Petic: Mi perfil</title>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->


 		<link href="assets/bootstrap/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		
		
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>
	<div id="miperfil-wrapper">
		<div class="row"><h3>Mi perfil</h3></div>
			<div id="miperfilcontainer" class="box container">
				<div class="row">
				   <div class="4u 12u(medium)">
				   		<section class="caja feature">
							<img src="images/usericon.png" />
							<div class="inner"><a href="editarperfil.php" id="botonedtarperfil" class="btn btn-sample" role="button">Editar perfil</a></div>
						</section>
					</div>

					 <div class="6u 12u(medium)">
					 	<section class = "caja feature">
					 		<div class="inner">
								<?php
									include("php/informacionmiperfil.php");
								?>
							</div>
						</section>
					  </div>

				   </div> <!-- end row -->

				   <div class="container">
				   <div class="row"><h4>Mis mascotas:</h4></div>
						<?php
							include("php/todasmismascotas.php");
						?>

				   </div>



				</div> <!-- end .main-col -->

			</div>

		</div>






				<footer>
					<?php
						include("footer.php");
					?>
				</footer>
	</body>

</html>
