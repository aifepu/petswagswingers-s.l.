<?php 
	include ("seguridad.php");
?>
<html>
	<head>
    <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/jquery.bxslider.css" />
		<link rel="stylesheet" href="assets/css/actividades.css" />
		<script type="text/javascript" src="assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="assets/js/acciones.js"></script>
		

 		<title>Petic: Actividad</title>
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>
		<div class="container">
			<button style="background-color: #ac474e; color: #fff; height: 60px; margin: 50px 32em; width: 230px;" onclick="darDeBaja('<?= $_SESSION["usuario"];?>'); return false;">Darse de baja</button>
		</div>
		<footer>
			<?php
				include("footer.php");
			?>
		</footer>
	</body>
</html>
