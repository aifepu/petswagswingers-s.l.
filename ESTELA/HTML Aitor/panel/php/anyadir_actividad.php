<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php include_once "conexion.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>

<body>

  <div class="titulo">
    <img src="../img/logo.png" width="25%" height="25%">
  </div>

  <nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
  </div>

  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Actividades <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="listar_actividades.php">Listar actividades</a></li>
          <li><a href="anyadir_actividad.php">Añadir actividad</a></li>
        </ul>
      </li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuarios <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="usuarios.php">Listar usuarios</a></li>
        </ul>
      </li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mascotas <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="mascotas.php">Listar mascotas</a></li>
        </ul>
      </li>

      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anuncios <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="anuncios.php">Listar anuncios</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>

    <div class="contenido">
      <form role="form" style="margin-left:1%" action="anyadir_actividad.php" method="post">
        <div class="form-group">
          <label for="plazastotalespersonas">Plazas para personas</label>
          <input type="number" class="form-control" name="plazastotalespersonas"
                 placeholder="Introduce cantidad de plazas para personas">
        </div>
        <div class="form-group">
          <label for="plazastotalesmascotas">Plazas para mascotas</label>
          <input type="number" class="form-control" name="plazastotalesmascotas"
                 placeholder="Introduce cantidad de plazas para mascotas">
        </div>
        <div class="form-group">
          <label for="precio">Precio de la actividad</label>
          <input type="number" class="form-control" name="precio"
                 placeholder="Introduce el precio de la actividad">
        </div>
        <div class="form-group">
          <label for="fechaInicio">Fecha de inicio</label>
          <input type="date" class="form-control" name="fechaInicio"
                 placeholder="Introduce la fecha de inicio de inscripción">
        </div>
        <div class="form-group">
          <label for="fechaFin">Fecha fin de inscripción</label>
          <input type="date" class="form-control" name="fechaFin"
                 placeholder="Introduce la fecha de fin de inscripción">
        </div>
        <div class="form-group">
          <label for="nombre">Nombre de la actividad</label>
          <input type="text" class="form-control" name="nombre"
                 placeholder="Introduce el nombre de la actividad">
        </div>
        <div class="form-group">
          <label for="descripcion">Descripción de la actividad</label>
          <input type="text" class="form-control" name="descripcion"
                 placeholder="Introduce la descripción de la actividad">
        </div>
        <div class="form-group">
          <label for="codCiudad">Codigo de la ciudad</label>
          <input type="text" class="form-control" name="fechaFin"
                 placeholder="Introduce la ciudad">
        </div>

        <div class="form-group">
          <label for="imagen">Adjuntar un archivo</label>
          <input type="file" id="ejemplo_archivo_1">
          <p class="help-block">Ejemplo de texto de ayuda.</p>
        </div>

        <button type="submit" class="btn btn-default">Enviar</button>
      </form>

      <?
      $plazastotalespersonas = $_POST['plazastotalespersonas'];
      $plazastotalesmascotas = $_POST['plazastotalesmascotas'];
      $precio = $_POST['precio'];
      $fechaInicio = $_POST['fechaInicio'];
      $fechaFin = $_POST['fechaFin'];
      $nombre = $_POST['nombre'];
      $descripcion = $_POST['descripcion'];

      $consulta = "insert into actividad (plazastotalespersona,plazastotalesmascota,precio,fechaInicio,fechaFin,
                      nombre,descripcion) values ('$plazastotalespersonas','$plazastotalesmascotas','$precio','$fechaInicio'
                                            ,'$fechaFin','$nombre','$descripcion')";
      $resultado = mysql_query($consulta);

      ?>
    </div>


</body>
</html>
