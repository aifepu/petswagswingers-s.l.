<!DOCTYPE html>
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="footer, address, phone, icons" />

	<link rel="stylesheet" href="css/footer.css">

	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">

</head>

		<footer class="footer-distributed">

			<div class="footer-left">
				<img src="images/footlogo.png" width="50%" height="50%">
				<h3>Busca, encuentra y disfruta de tu manada</h3>
				<p class="footer-company-name">PETIC &copy; 2016</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>Universidad de Alicante</span> Alicante, España</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p> 966 312 987</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:admin@petic.com">admin@petic.com</a></p>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>Nos dedicamos a querer a nuestros animales</span>
					Para todos aquellos que quieran a su mascota tanto como a la vida misma, esta es su red social. Contactos entre amantes de los animales.
				</p>

				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>

				</div>
			</div>
		</footer>
</html>
