<?php
	include ("seguridad.php");
	if($_SESSION['nick']!=$_GET["dueno"]){
		$mimascota='no';
	}else{
		$mimascota='yes';
	}
?>
<html>
	<head>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/jquery.bxslider.css" />
		<link rel="stylesheet" href="assets/css/font-awesome.min.css" />
		<link rel="stylesheet" href="assets/css/mascotas.css" />
		        <link rel="stylesheet" href="registroassets/font-awesome/css/font-awesome.min.css">
		

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
 		<title>Petic: Pagina de Inicio</title>
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>
		<?php

			include_once "php/conexion.php";
			$dueno=$_GET["dueno"];
			$nombre=$_GET["nombre"];
			$consulta = "SELECT m.nombre nombre, m.fechaNacimiento fechaNacimiento, m.fotografia fotografia, m.descripcion descripcion, m.sexo sexo, r.nombre raza, c.nombre ciudad, p.nombre provincia, m.dueno email, u.nombre nombreusuario, u.apellidos apellidos, u.nick nick
				FROM mascota m INNER JOIN raza r ON r.codigo=m.codraza
			 	INNER JOIN usuario u ON m.dueno=u.email
			 	LEFT JOIN ciudad c ON u.codciudad=c.codigo
			 	LEFT JOIN provincia p ON c.codigoprovincia=p.codigo
			 	WHERE m.nombre='$nombre' and u.nick='$dueno'";
			$resultado = mysql_query($consulta);
		    while ($row = mysql_fetch_array($resultado)) {
		                  unset($fechaNacimiento, $fotografia, $descripcion, $sexo, $raza, $ciudad, $provincia, $email, $nick);
		                  $nombre = $row['nombre'];
		                  $fechaNacimiento = $row['fechaNacimiento'];
		                  $fotografia = $row['fotografia']; 
		                  $descripcion = $row['descripcion'];
		                  $sexo = $row['sexo'];
		                  $raza = $row['raza'];
		                  $ciudad = $row['ciudad'];
		                  $provincia = $row['provincia'];
		                  $email = $row['email'];
		                  $nick = $row['nick'];

			}
			$consulta="SELECT calcularedad('$nombre','$email') edad";
			$resultado = mysql_query($consulta);
		    while ($row = mysql_fetch_array($resultado)) {
		    	unset($edad);
		    	$edad = $row['edad'];
		    }
			
		?> 
		<div class="container">
			<div id="infomascota-wrapper">
				<div id="cajaiz">
					<div id="cajaimagen">
						<img height="100%" width="100%" src="data:image;base64,<?=$fotografia?>">
					</div>
					<div id="cajadatos">
						<p>Edad: <?=$edad.' a&ntilde;os';?> </p>
					<p>Sexo: <?=$sexo;?></p>
					<p>Raza: <?=$raza;?></p>
					<p>Ciudad: <?=$ciudad;?></p>
					<p>Provincia: <?=$provincia;?></p>
					</div>
				</div>
				<div id="cajade">
					<h1><?=$nombre;?></h1>
					<div id="cajasobremi">
					<?=$descripcion;?>
					</div>
					<div id="cajadueno">
					<?
					$link="perfil.php?nick=".$dueno;
					?>
						<div>
							<button onclick="window.location.href='<?=$link;?>'" id="perfil">
									<div style="margin-right: 20px;">
										<i class="fa fa-user fa-3x" aria-hidden="true"></i>
									</div>
									<div style="vertical-align: top; margin-top:10%;">
										<p><?=$nick;?></p>
									</div>
							</button>
						</div>
						<div id="mimascota" style="display:<?if($mimascota=='yes'){echo '';}else{echo 'none';}?>">
							<button onclick="window.location.href='principal.php'" id="perfil"><i class="fa fa-wrench fa-2x" aria-hidden="true"></i></a>
							<button onclick="window.location.href='principal.php'" id="perfil"><i class="fa fa-close fa-2x" aria-hidden="true"></i></a>
								<?php
									$ma="matching.php?nombre=".$nombre."&dueno=".$dueno;
								?>
						
							<button class="btn btn-sample" onclick="window.location.href='<?=$ma;?>'" id="matching">Compatibilidad</button>
				
						</div>

						</button>
					</div>
				</div>
			</div>
		</div>

	<!-- quizas -->
		<div id="interes-wrapper">
			<header>
				<h3>Quiz&aacute;s te interesen</h3>
			</header>
			<div class="spotlights">
				<ul class="bxslider">
					<?
					include_once "php/conexion.php";
					$consulta="SELECT nombre, descripcion, fotografia FROM mascota";

					$query=mysql_query($consulta);
					if(mysql_num_rows($query)>0){
						?>
						<li>
						<?
						$cont16=0;
						$cont4=0;
						while(($fila=mysql_fetch_array($query)) and ($cont16<16)){
							$nombre=$fila["nombre"];
							$descripcion=$fila["descripcion"];
							$fotografia=$fila["fotografia"];

							if($cont4==4){
								$cont4=0;
								?>
								</li>
								<li>
								<?
							}
							if($cont4<5){
								?>
								<div class="caja-mascota" >
									<?=$nombre;?>
								</div>
								<?
								$cont4++;
							}
							$cont16++;
						}
						?>
						</li>
						<?
					}else{
						echo "Error: no existen datos";
					}
					?>
				</ul>

			</div>
		</div>

		<footer>
			<?php
				include("footer.php");
			?>
		</footer>
		<script src="assets/js/jquery.bxslider.js"></script>
		<script>
		$(document).ready(function(){
		  $('.bxslider').bxSlider();
		});
		</script>
	</body>

</html>