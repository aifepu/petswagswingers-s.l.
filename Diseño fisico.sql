CREATE TABLE IF NOT EXISTS provincia (codigo smallint unsigned AUTO_INCREMENT, nombre varchar(30), CONSTRAINT pk_provincia PRIMARY KEY (codigo))
ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ciudad(codigo smallint unsigned AUTO_INCREMENT, nombre varchar(30), codigoProvincia smallint unsigned NOT NULL, CONSTRAINT pk_ciudad PRIMARY KEY (codigo), CONSTRAINT fk_ciudadprovincia FOREIGN KEY (codigoProvincia) REFERENCES provincia(codigo) ON UPDATE CASCADE ON DELETE CASCADE)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS ciudadMem(codigo int(11) DEFAULT NULL,
nombre varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
codigoprovincia int(11) DEFAULT NULL) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


CREATE TABLE IF NOT EXISTS admin(user varchar(20), password varchar(16), CONSTRAINT pk_admin PRIMARY KEY(user)) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS categoriaanimal (codigo tinyint unsigned AUTO_INCREMENT, nombre varchar (30),CONSTRAINT pk_categoriaanimal PRIMARY KEY (codigo)) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS raza (codigo smallint unsigned AUTO_INCREMENT, nombre varchar(30), tamanyo enum('Grande','Mediano','Pequeno'), categoriaAnimal tinyint unsigned NOT NULL, CONSTRAINT pk_raza PRIMARY KEY(codigo), CONSTRAINT fk_razacategoria FOREIGN KEY (categoriaAnimal) REFERENCES categoriaanimal(codigo)ON UPDATE CASCADE ON DELETE CASCADE)ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS usuario(email varchar(50), nick varchar(30) NOT NULL, nombre varchar(30), apellidos varchar(50), contrasena varchar(16), fecha_alta datetime, estado enum('Activo','Inactivo') NOT NULL, codciudad smallint unsigned, CONSTRAINT pk_usuario PRIMARY KEY (email), CONSTRAINT ak_usuario UNIQUE(nick), CONSTRAINT fk_usuariociudad FOREIGN KEY (codciudad) REFERENCES ciudad(codigo)ON UPDATE CASCADE ON DELETE SET NULL) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS mascota(nombre varchar(30), fechaNacimiento date, fotografia mediumblob, descripcion varchar(200), sexo enum('Macho','Hembra'), codraza smallint unsigned, dueno varchar(50) NOT NULL, estado enum('Activo','Inactivo') NOT NULL, CONSTRAINT pk_mascota PRIMARY KEY (dueno,nombre), CONSTRAINT fk_mascotaraza FOREIGN KEY (codraza) REFERENCES raza(codigo) ON UPDATE CASCADE ON DELETE SET NULL, CONSTRAINT fk_mascotausuario FOREIGN KEY (dueno) REFERENCES usuario(email) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS favorito(usuario1 varchar(50), mascota1 varchar (30), dueno1 varchar(50), CONSTRAINT pk_favorito PRIMARY KEY (usuario1, mascota1, dueno1), CONSTRAINT fk_favusuario FOREIGN KEY (usuario1) REFERENCES usuario(email) ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT fk_favmascota FOREIGN KEY (dueno1, mascota1) REFERENCES mascota(dueno, nombre)ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS anuncio (codigo smallint unsigned AUTO_INCREMENT, titulo varchar(30), contenido varchar(300), autor varchar(50) NOT NULL, fecha datetime,CONSTRAINT pk_anuncio PRIMARY KEY (codigo),
CONSTRAINT fk_anuncioautor FOREIGN KEY (autor) REFERENCES
usuario(email) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS matching (nombreMascota1 varchar(30),
dueno1 varchar(50), nombreMascota2 varchar(30), dueno2 varchar(50), fecha datetime, CONSTRAINT pk_matching PRIMARY KEY (nombreMascota1, dueno1, nombreMascota2, dueno2),
CONSTRAINT fk_matching1 FOREIGN KEY (dueno1, nombreMascota1) REFERENCES
mascota(dueno, nombre) ON UPDATE CASCADE ON DELETE CASCADE,
CONSTRAINT fk_matching2 FOREIGN KEY (dueno2, nombreMascota2) REFERENCES
mascota(dueno, nombre) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS anunciovisibleporprovincia(codigoAnuncio smallint unsigned, codigoProvincia smallint unsigned, CONSTRAINT pk_anunciovisibleporprovincia PRIMARY KEY(codigoAnuncio, codigoProvincia), CONSTRAINT fk_anunciovisibleporprovincia_anuncio FOREIGN KEY (codigoAnuncio) REFERENCES anuncio(codigo)ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT fk_anunciovisibleporprovincia_provincia FOREIGN KEY (codigoProvincia) REFERENCES provincia(codigo) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=innoDB;


CREATE TABLE IF NOT EXISTS anunciovisibleporcategoria (codigoCategoria tinyint unsigned, codigoAnuncio smallint unsigned, CONSTRAINT pk_anunciocat PRIMARY KEY (codigoCategoria, codigoAnuncio), CONSTRAINT fk_codcat FOREIGN KEY (codigoCategoria) REFERENCES categoriaanimal(codigo) ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT fk_codanu FOREIGN KEY (codigoAnuncio) REFERENCES anuncio(codigo) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS categoriamensaje (codigo tinyint unsigned AUTO_INCREMENT, nombre varchar (30),CONSTRAINT pk_categoriamensaje PRIMARY KEY (codigo)) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS mensajeatencionalcliente (codigo tinyint unsigned AUTO_INCREMENT, fecha datetime, asunto varchar (50), contenido mediumtext,
estado enum('Sin contestar','Contestado','Solucionado'), usuario varchar(50), codcategoriamensaje tinyint unsigned, CONSTRAINT pk_mensajeac PRIMARY KEY (codigo), CONSTRAINT fk_mensajeacusuario FOREIGN KEY(usuario) REFERENCES usuario(email) ON UPDATE CASCADE ON DELETE SET NULL, CONSTRAINT fk_mensajeaccategoriam FOREIGN KEY(codcategoriamensaje) REFERENCES categoriamensaje(codigo) ON UPDATE CASCADE ON DELETE SET NULL) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS amistad (usuario1 varchar(50), usuario2 varchar(50), fecha datetime, estado enum('Pendiente','Aceptada','Rechazada'),
CONSTRAINT pk_amistad PRIMARY KEY (usuario1,usuario2),
CONSTRAINT fk_amistad1 FOREIGN KEY (usuario1) REFERENCES
usuario(email) ON UPDATE CASCADE ON DELETE CASCADE,
CONSTRAINT fk_amistad2 FOREIGN KEY (usuario2) REFERENCES
usuario(email) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS actividad(codigo tinyint unsigned AUTO_INCREMENT, plazastotalespersona tinyint unsigned, plazasocupadaspersona tinyint unsigned, plazasocupadasmascota tinyint unsigned, plazastotalesmascota tinyint unsigned, precio decimal, fechaInicio datetime, fechaFin datetime, nombre varchar(30), descripcion mediumtext, imagen mediumblob, codciudad smallint unsigned, codadmin varchar(20), CONSTRAINT pk_actividad PRIMARY KEY (codigo), CONSTRAINT fk_actividadciudad FOREIGN KEY (codciudad) REFERENCES ciudad(codigo)ON UPDATE CASCADE ON DELETE SET NULL, CONSTRAINT fk_actividadadmin FOREIGN KEY(codadmin) REFERENCES admin(user) ON UPDATE CASCADE ON DELETE SET NULL) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS reserva (usuario1 varchar(50), actividad1 tinyint unsigned, plazasreservadaspersona tinyint unsigned, plazasreservadasmascota tinyint unsigned,
CONSTRAINT pk_reserva PRIMARY KEY (usuario1, actividad1),
CONSTRAINT fk_reserva1 FOREIGN KEY (usuario1) REFERENCES
usuario(email) ON UPDATE CASCADE ON DELETE CASCADE,
CONSTRAINT fk_reserva2 FOREIGN KEY (actividad1) REFERENCES
actividad(codigo) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS cita(nombreMascota1 varchar(30),
dueno1 varchar(50), nombreMascota2 varchar(30), dueno2 varchar(50), fecha datetime, valoracion decimal(10,0), CONSTRAINT pk_cita PRIMARY KEY (nombreMascota1, dueno1, nombreMascota2, dueno2),
CONSTRAINT fk_cita1 FOREIGN KEY (dueno1, nombreMascota1) REFERENCES
mascota(dueno, nombre) ON UPDATE CASCADE ON DELETE CASCADE,
CONSTRAINT fk_cita2 FOREIGN KEY (dueno2, nombreMascota2) REFERENCES
mascota(dueno, nombre) ON UPDATE CASCADE ON DELETE CASCADE) ENGINE=InnoDB;


