function eliminaMascota(nick, nombre, email){
	var accion="eliminar";
	if(confirm("¿Estás seguro?")){
		$.post("ajax/_ajaxMascota.php",{nombre: nombre, email: email, accion: accion}, function(data){
			if(data=="Eliminado"){
				alert(data);
				window.location="perfil.php?nick="+nick;
			}else{
				alert(data);
				window.location="darsedebaja.php";
			}
	   	});
	}
}

function darDeBaja(email){
	if(confirm("¿Estás seguro?")){
		$.post("ajax/_ajaxUsuario.php",{email: email}, function(data){
	      	window.location="index.php";
	   	});
	}
}
