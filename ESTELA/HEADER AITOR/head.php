<!DOCTYPE HTML>
<link rel="stylesheet" href="css/bootstrap.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<html>
	<head>
        <meta charset="UTF-8"/>
		<title>Petic: Pagina de Inicio</title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />

	</head>
	<body>
						<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse"
												data-target=".navbar-ex1-collapse">
									<span class="sr-only">Desplegar navegación</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<a href="head.html" style="margin-right:20px;"><img src="images/brand.png"alt=""/></a>
							</div>

							<!-- Agrupar los enlaces de navegación, los formularios y cualquier
									 otro elemento que se pueda ocultar al minimizar la barra -->
							<div class="collapse navbar-collapse navbar-ex1-collapse">
								<ul class="nav navbar-nav">
									<li><a style="height:54px;" href="#">Inicio</a></li>
									<li><a style="height:54px;" href="#">Mascotas</a></li>
									<li><a style="height:54px;" href="#">Anuncios</a></li>
									<li><a style="height:54px;" href="#">Actividades</a></li>
									<li><a style="height:54px;" href="#">Mi perfil</a></li>
									<li><a style="height:54px;" href="#">Contacto</a></li>
									<li style="height:50px;margin-left:20px;"><a href="#"><img src="images/actividad.png"/></a></li>
									<li style="height:50px;margin-left:20px;"><a href="#"><img src="images/amistad.png"/></a></li>
								</ul>


								<form class="navbar-form navbar-right" role="search">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Buscar">
									</div>
									<button type="submit" class="btn btn-default" style="margin-right:10px;"><i class="fa fa-search"></i></button>
									<button class="btn btn-default" style="margin-right:10px;" href="#">Cerrar sesión</a>
								</form>
							</div>
							</nav>

							<div class="cabecera" style="border-bottom-width:2px;border-bottom-color: #ac474e;border-bottom-style: solid;">
								<div class="row" style="margin-top:60px; margin-left: 12px;">
                	<div class="col-md-4" >
                  	<a href="head.html"><img src="images/cabecera.png" alt=""/></a>
                	</div>
								</div>
							</div>

						</body>
						</html>
