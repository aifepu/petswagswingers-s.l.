<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Acceder a Petic</title>
    <link rel="stylesheet" href="assets/css/login.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  </head>

  <body>

    <form method = "post" action="php/comprobar_login.php">
	<div class="login">
		<div class="login-screen">
             <div id="logo">
			<b><img src="images/logo.png" alt="" width="200" height="108" /></b>
			</div>
			<div class="app-title">
				<h1>Iniciar sesión</h1>
				<?php 
						if(isset($_GET["error"])){
							echo "<span>Error en la autenticación</span>";
						}
				?>


			</div>

			<div class="login-form">
        <form method = "post" action="php/comprobar_login.php">
				<div class="control-group">
				<input required type="text" name="login-name" class="login-field" value="" placeholder="Usuario" id="login-name">
				<label class="login-field-icon fui-user" for="login-name"></label>
				</div>

				<div class="control-group">
				<input required type="password" name="login-pass" class="login-field" value="" placeholder="Contraseña" id="login-pass">
				<label class="login-field-icon fui-lock" for="login-pass"></label>
				</div>

				<input type="submit" class="btn btn-primary btn-large btn-block" href="#"></input>

				<a class="login-link" href="registro.php">¿No tienes cuenta?¡Regístrate!</a>
      </form>
			</div>
		</div>
	</div>
       </form>


  </body>
</html>
