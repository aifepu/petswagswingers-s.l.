-- --------------------------------------------------------
-- Host:                         bbdd.dlsi.ua.es
-- Versión del servidor:         5.1.73-1+deb6u1-log - (Debian)
-- SO del servidor:              debian-linux-gnu
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla gi_mascotas.actividad
CREATE TABLE IF NOT EXISTS `actividad` (
  `codigo` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `plazastotales` tinyint(3) unsigned DEFAULT NULL,
  `plazasocupadas` tinyint(3) unsigned DEFAULT NULL,
  `precio` decimal(10,0) DEFAULT NULL,
  `fechaInicio` datetime DEFAULT NULL,
  `fechaFin` datetime DEFAULT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` mediumtext COLLATE utf8_spanish_ci,
  `codciudad` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_actividadciudad` (`codciudad`),
  CONSTRAINT `fk_actividadciudad` FOREIGN KEY (`codciudad`) REFERENCES `ciudad` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.amistad
CREATE TABLE IF NOT EXISTS `amistad` (
  `usuario1` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `usuario2` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `fecha` datetime DEFAULT NULL,
  `estado` enum('Pendiente','Aceptada','Rechazada') COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`usuario1`,`usuario2`),
  KEY `fk_amistad2` (`usuario2`),
  CONSTRAINT `fk_amistad1` FOREIGN KEY (`usuario1`) REFERENCES `usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_amistad2` FOREIGN KEY (`usuario2`) REFERENCES `usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.anuncio
CREATE TABLE IF NOT EXISTS `anuncio` (
  `codigo` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `contenido` varchar(300) COLLATE utf8_spanish_ci DEFAULT NULL,
  `autor` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_anuncioautor` (`autor`),
  CONSTRAINT `fk_anuncioautor` FOREIGN KEY (`autor`) REFERENCES `usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.anunciovisibleporcategoria
CREATE TABLE IF NOT EXISTS `anunciovisibleporcategoria` (
  `codigoCategoria` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `codigoAnuncio` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigoCategoria`,`codigoAnuncio`),
  KEY `fk_codanu` (`codigoAnuncio`),
  CONSTRAINT `fk_codcat` FOREIGN KEY (`codigoCategoria`) REFERENCES `categoriaanimal` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_codanu` FOREIGN KEY (`codigoAnuncio`) REFERENCES `anuncio` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.anunciovisibleporciudad
CREATE TABLE IF NOT EXISTS `anunciovisibleporciudad` (
  `codigoAnuncio` smallint(5) unsigned NOT NULL DEFAULT '0',
  `codigoCiudad` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`codigoAnuncio`,`codigoCiudad`),
  KEY `fk_anunciovisibleporciudad_ciudad` (`codigoCiudad`),
  CONSTRAINT `fk_anunciovisibleporciudad_anuncio` FOREIGN KEY (`codigoAnuncio`) REFERENCES `anuncio` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_anunciovisibleporciudad_ciudad` FOREIGN KEY (`codigoCiudad`) REFERENCES `ciudad` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para evento gi_mascotas.borrar_solicitudes_rechazadas
DELIMITER //
CREATE EVENT `borrar_solicitudes_rechazadas` ON SCHEDULE EVERY 1 WEEK STARTS '2016-03-14 00:26:12' ON COMPLETION PRESERVE ENABLE DO DELETE FROM amistad WHERE estado='Rechazada'//
DELIMITER ;


-- Volcando estructura para función gi_mascotas.calcularedad
DELIMITER //
CREATE DEFINER=`gi_shm11`@`%` FUNCTION `calcularedad`(elnombre varchar(30),eldueno varchar(50)) RETURNS int(11)
BEGIN
DECLARE edad, anyoactual, anyonacimiento int;

SELECT YEAR(fechaNacimiento) INTO anyonacimiento FROM mascota WHERE nombre=elnombre AND dueno=eldueno;

SELECT YEAR(NOW()) INTO anyoactual;

SET edad = anyoactual-anyonacimiento;

RETURN edad;
END//
DELIMITER ;


-- Volcando estructura para tabla gi_mascotas.categoriaanimal
CREATE TABLE IF NOT EXISTS `categoriaanimal` (
  `codigo` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.categoriamensaje
CREATE TABLE IF NOT EXISTS `categoriamensaje` (
  `codigo` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.cita
CREATE TABLE IF NOT EXISTS `cita` (
  `nombreMascota1` varchar(30) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `dueno1` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `nombreMascota2` varchar(30) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `dueno2` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `fecha` datetime DEFAULT NULL,
  `valoracion` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`nombreMascota1`,`dueno1`,`nombreMascota2`,`dueno2`),
  KEY `fk_cita1` (`dueno1`,`nombreMascota1`),
  KEY `fk_cita2` (`dueno2`,`nombreMascota2`),
  CONSTRAINT `fk_cita1` FOREIGN KEY (`dueno1`, `nombreMascota1`) REFERENCES `mascota` (`dueno`, `nombre`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_cita2` FOREIGN KEY (`dueno2`, `nombreMascota2`) REFERENCES `mascota` (`dueno`, `nombre`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.ciudad
CREATE TABLE IF NOT EXISTS `ciudad` (
  `codigo` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigoProvincia` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_ciudadprovincia` (`codigoProvincia`),
  CONSTRAINT `fk_ciudadprovincia` FOREIGN KEY (`codigoProvincia`) REFERENCES `provincia` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para procedimiento gi_mascotas.compatibilidad_mascotas
DELIMITER //
CREATE DEFINER=`gi_shm11`@`%` PROCEDURE `compatibilidad_mascotas`(in eldueno varchar(50), in elnombre varchar(30), in laraza smallint unsigned, in elsexo enum('Macho','Hembra'), in laciudad smallint unsigned)
BEGIN
	DECLARE done BOOL DEFAULT 0;
	DECLARE auxdueno varchar(50);
	DECLARE auxnombre varchar(30);
	DECLARE auxciudad smallint unsigned;

-- Cursos que recoge solo el nombre y dueño de las mascotas 
-- que cumplen que no son dle mismo dueño, ni sexo y tienen
-- misma raza.
DECLARE cur1 CURSOR FOR SELECT dueno, nombre FROM mascota WHERE dueno!=eldueno AND sexo!=elsexo AND raza=laraza;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done=1;
	
	OPEN cur1;
	REPEAT
		FETCH cur1 INTO auxdueno,auxnombre;
		IF NOT done THEN
			-- Seleccionamos la ciudad de la mascota que se 				-- esta recogiendo en este paso del cursor
			SELECT ciudad INTO auxciudad FROM usuario, 					mascota WHERE auxdueno=dueno AND 						auxnombre=mascota.nombre AND email=dueno;
			
			CASE
				when(auxciudad=laciudad) THEN INSERT IGNORE 				INTO matching VALUES(elnombre,eldueno, 					auxnombre,auxdueno, NOW());
		  	END CASE;
		END IF;
	UNTIL done END REPEAT;
	CLOSE cur1;

END//
DELIMITER ;


-- Volcando estructura para función gi_mascotas.comprobarultimamascota
DELIMITER //
CREATE DEFINER=`gi_shm11`@`%` FUNCTION `comprobarultimamascota`(elemail varchar(50)) RETURNS tinyint(1)
BEGIN

	DECLARE cuantos tinyint;
	DECLARE ultima BOOL DEFAULT 0;

SELECT count(*) INTO cuantos FROM mascota where mascota.dueno=elemail;

IF cuantos<2 THEN SET ultima=1;
	END IF;
RETURN ultima;
END//
DELIMITER ;


-- Volcando estructura para tabla gi_mascotas.favorito
CREATE TABLE IF NOT EXISTS `favorito` (
  `usuario1` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `mascota1` varchar(30) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `dueno1` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`usuario1`,`mascota1`,`dueno1`),
  KEY `fk_favmascota` (`dueno1`,`mascota1`),
  CONSTRAINT `fk_favusuario` FOREIGN KEY (`usuario1`) REFERENCES `usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_favmascota` FOREIGN KEY (`dueno1`, `mascota1`) REFERENCES `mascota` (`dueno`, `nombre`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para procedimiento gi_mascotas.insertar_cita
DELIMITER //
CREATE DEFINER=`gi_shm11`@`%` PROCEDURE `insertar_cita`(in lamascota1 varchar(30), in eldueno1 varchar(50), in lamascota2 varchar(30), in eldueno2 varchar(50), in lafecha datetime, in lavaloracion decimal)
BEGIN

DECLARE mediavaloracion, valoracionOld decimal;
DECLARE cuantos int;


SELECT count(*) INTO cuantos FROM cita WHERE nombreMascota1 = lamascota1 AND dueno1 = eldueno1 AND nombreMascota2 = lamascota2 AND dueno2 = eldueno2 AND fecha=lafecha;

SELECT valoracion INTO valoracionOld FROM cita WHERE nombreMascota1 = lamascota1 AND dueno1 = eldueno1 AND nombreMascota2 = lamascota2 AND dueno2 = eldueno2 AND fecha=lafecha;

SET mediavaloracion = (valoracionOld+lavaloracion)/2;

IF cuantos<1 THEN INSERT INTO cita VALUES(lamascota1,eldueno1, lamascota2,eldueno2,lafecha,lavaloracion);

ELSE UPDATE  cita SET valoracion = mediavaloracion WHERE nombreMascota1 = lamascota1 AND dueno1 = eldueno1 AND nombreMascota2 = lamascota2 AND dueno2 = eldueno2 AND fecha=lafecha;
END IF;

END//
DELIMITER ;


-- Volcando estructura para tabla gi_mascotas.mascota
CREATE TABLE IF NOT EXISTS `mascota` (
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `fechaNacimiento` date DEFAULT NULL,
  `fotografia` blob,
  `descripcion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sexo` enum('Macho','Hembra') COLLATE utf8_spanish_ci DEFAULT NULL,
  `codraza` smallint(5) unsigned DEFAULT NULL,
  `dueno` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `estado` enum('Activo','Inactivo') COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`dueno`,`nombre`),
  KEY `fk_mascotaraza` (`codraza`),
  CONSTRAINT `fk_mascotaraza` FOREIGN KEY (`codraza`) REFERENCES `raza` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_mascotausuario` FOREIGN KEY (`dueno`) REFERENCES `usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.matching
CREATE TABLE IF NOT EXISTS `matching` (
  `nombreMascota1` varchar(30) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `dueno1` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `nombreMascota2` varchar(30) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `dueno2` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`nombreMascota1`,`dueno1`,`nombreMascota2`,`dueno2`),
  KEY `fk_matching1` (`dueno1`,`nombreMascota1`),
  KEY `fk_matching2` (`dueno2`,`nombreMascota2`),
  CONSTRAINT `fk_matching1` FOREIGN KEY (`dueno1`, `nombreMascota1`) REFERENCES `mascota` (`dueno`, `nombre`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_matching2` FOREIGN KEY (`dueno2`, `nombreMascota2`) REFERENCES `mascota` (`dueno`, `nombre`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.mensajeatencionalcliente
CREATE TABLE IF NOT EXISTS `mensajeatencionalcliente` (
  `codigo` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `asunto` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `contenido` mediumtext COLLATE utf8_spanish_ci,
  `estado` enum('Sin contestar','Contestado','Solucionado') COLLATE utf8_spanish_ci DEFAULT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codcategoriamensaje` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_mensajeacusuario` (`usuario`),
  KEY `fk_mensajeaccategoriam` (`codcategoriamensaje`),
  CONSTRAINT `fk_mensajeacusuario` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`email`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_mensajeaccategoriam` FOREIGN KEY (`codcategoriamensaje`) REFERENCES `categoriamensaje` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para función gi_mascotas.plazaslibres
DELIMITER //
CREATE DEFINER=`gi_shm11`@`%` FUNCTION `plazaslibres`(laactividad tinyint unsigned) RETURNS tinyint(3) unsigned
BEGIN
	DECLARE auxplazaslibres, auxplazastotales, auxplazasocupadas 	tinyint;

	SELECT plazastotales INTO auxplazastotales FROM actividad 	WHERE codigo=laactividad;

SELECT plazasocupadas INTO auxplazasocupadas FROM actividad WHERE codigo=laactividad;

SET auxplazaslibres = auxplazastotales - auxplazasocupadas;

RETURN auxplazaslibres;
END//
DELIMITER ;


-- Volcando estructura para tabla gi_mascotas.provincia
CREATE TABLE IF NOT EXISTS `provincia` (
  `codigo` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.raza
CREATE TABLE IF NOT EXISTS `raza` (
  `codigo` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tamanyo` enum('Grande','Mediano','Pequeño') COLLATE utf8_spanish_ci DEFAULT NULL,
  `categoriaAnimal` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`codigo`),
  KEY `fk_razacategoria` (`categoriaAnimal`),
  CONSTRAINT `fk_razacategoria` FOREIGN KEY (`categoriaAnimal`) REFERENCES `categoriaanimal` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.reserva
CREATE TABLE IF NOT EXISTS `reserva` (
  `usuario1` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `actividad1` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `plazasreservadas` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`usuario1`,`actividad1`),
  KEY `fk_reserva2` (`actividad1`),
  CONSTRAINT `fk_reserva1` FOREIGN KEY (`usuario1`) REFERENCES `usuario` (`email`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_reserva2` FOREIGN KEY (`actividad1`) REFERENCES `actividad` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla gi_mascotas.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `email` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `nick` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `contrasena` varchar(16) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  `estado` enum('Activo','Inactivo') COLLATE utf8_spanish_ci NOT NULL,
  `codciudad` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `ak_usuario` (`nick`),
  KEY `fk_usuariociudad` (`codciudad`),
  CONSTRAINT `fk_usuariociudad` FOREIGN KEY (`codciudad`) REFERENCES `ciudad` (`codigo`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para disparador gi_mascotas.amistad_BI_fechaamistad
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY';
DELIMITER //
CREATE TRIGGER `amistad_BI_fechaamistad` BEFORE INSERT ON `amistad` FOR EACH ROW SET NEW.fecha=NOW()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Volcando estructura para disparador gi_mascotas.anuncio_BI_fecha_publicacion
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY';
DELIMITER //
CREATE TRIGGER `anuncio_BI_fecha_publicacion` BEFORE INSERT ON `anuncio` FOR EACH ROW SET NEW.fecha=NOW()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Volcando estructura para disparador gi_mascotas.estadomascota_AU_usuario
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY';
DELIMITER //
CREATE TRIGGER `estadomascota_AU_usuario` AFTER UPDATE ON `usuario` FOR EACH ROW BEGIN
	IF OLD.estado!=NEW.estado THEN 
		UPDATE mascota SET estado=NEW.estado WHERE 					NEW.email=mascota.dueno;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Volcando estructura para disparador gi_mascotas.mensajeatencionalcliente_BI_fechaenvio
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY';
DELIMITER //
CREATE TRIGGER `mensajeatencionalcliente_BI_fechaenvio` BEFORE INSERT ON `mensajeatencionalcliente` FOR EACH ROW SET NEW.fecha=NOW()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Volcando estructura para disparador gi_mascotas.reserva_AI_incrementarplazasocupadas
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY';
DELIMITER //
CREATE TRIGGER `reserva_AI_incrementarplazasocupadas` AFTER INSERT ON `reserva` FOR EACH ROW BEGIN
	DECLARE plazasnuevas int;
	DECLARE plazasocu int;

	SET plazasnuevas = NEW.plazasreservadas;

	SELECT plazasocupadas INTO plazasocu FROM actividad WHERE 	codigo=NEW.actividad1;

	UPDATE actividad SET plazasocupadas=plazasnuevas+plazasocu
	WHERE codigo=NEW.actividad1;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Volcando estructura para disparador gi_mascotas.reserva_AU_actualizarplazasocupadas
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY';
DELIMITER //
CREATE TRIGGER `reserva_AU_actualizarplazasocupadas` AFTER UPDATE ON `reserva` FOR EACH ROW BEGIN
	DECLARE plazasreservadasA, plazasreservadasD, plazasnuevas int;
	DECLARE plazasocu int;

	SET plazasreservadasA = OLD.plazasreservadas;
	SET plazasreservadasD = NEW.plazasreservadas;
	
	-- Si la operacion da positivo, significa que se han 
	-- incrementado las plazas ocupadas y si da negativo que han 
	-- disminuido 
	SET plazasnuevas = plazasreservadasD-plazasreservadasA;

	SELECT plazasocupadas INTO plazasocu FROM actividad WHERE 	codigo=OLD.actividad1;

	UPDATE actividad SET plazasocupadas=plazasocu+plazasnuevas 	WHERE codigo=OLD.actividad1;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Volcando estructura para disparador gi_mascotas.reserva_BD_decrementarplazasocupadas
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY';
DELIMITER //
CREATE TRIGGER `reserva_BD_decrementarplazasocupadas` BEFORE DELETE ON `reserva` FOR EACH ROW BEGIN
	DECLARE plazasnuevas int;
	DECLARE plazasocu int;

	SET plazasnuevas = OLD.plazasreservadas;

	SELECT plazasocupadas INTO plazasocu FROM actividad WHERE 	codigo=OLD.actividad1;

	UPDATE actividad SET plazasocupadas=plazasocu-plazasnuevas 	WHERE codigo=OLD.actividad1;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Volcando estructura para disparador gi_mascotas.usuario_BI_fecha_alta
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY';
DELIMITER //
CREATE TRIGGER `usuario_BI_fecha_alta` BEFORE INSERT ON `usuario` FOR EACH ROW SET NEW.fecha_alta=NOW()//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
