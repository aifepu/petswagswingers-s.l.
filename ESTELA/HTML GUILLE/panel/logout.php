<?php
 //Crear sesión
 session_start();
 //Vaciar sesión
 $_SESSION = NULL;
 //Destruir Sesión
 session_destroy();
 //Redireccionar a login.php
 header("location: index.php");
?>
