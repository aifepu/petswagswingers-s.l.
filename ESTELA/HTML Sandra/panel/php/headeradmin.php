<div class="titulo">
<img src="../img/logo.png" width="25%" height="25%">
</div>

<nav class="navbar navbar-default" role="navigation">
<!-- El logotipo y el icono que despliega el menú se agrupan
     para mostrarlos mejor en los dispositivos móviles -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle" data-toggle="collapse"
          data-target=".navbar-ex1-collapse">
    <span class="sr-only">Desplegar navegación</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
</div>

<!-- Agrupar los enlaces de navegación, los formularios y cualquier
     otro elemento que se pueda ocultar al minimizar la barra -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
  <ul class="nav navbar-nav">
    <li>
      <a href="../panel_admin.php">Inicio admin</a>
    </li>
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Actividades <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="listar_actividades.php">Listar actividades</a></li>
        <li><a href="anyadir_actividad.php">Añadir actividad</a></li>
      </ul>
    </li>

    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuarios <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="usuarios.php">Listar usuarios</a></li>
        <li><a href="anyadir_usuario.php">Añadir usuario</a></li>
      </ul>
    </li>

    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mascotas <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="mascotas.php">Listar mascotas</a></li>
        <li><a href="anyadir_mascota.php">Añadir mascota</a></li>
      </ul>
    </li>

    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anuncios <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="anuncios.php">Listar anuncios</a></li>
        <li><a href="anyadir_anuncio.php">Añadir anuncio</a></li>
      </ul>
    </li>

    <li>
      <a href="../logout.php">Cerrar Sesión</a>
    </li>
  </ul>
</div>
</nav>
