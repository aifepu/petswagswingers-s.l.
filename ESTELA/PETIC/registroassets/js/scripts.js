
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    $.backstretch("registroassets/img/backgrounds/1.jpg");
    
    $('#top-navbar-1').on('shown.bs.collapse', function(){
    	$.backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
    	$.backstretch("resize");
    });
    
    /*
        Form
    */
    $('.registration-form fieldset:first-child').fadeIn('slow');
    
    $('.registration-form input[type="text"], .registration-form input[type="password"], .registration-form select[class="registerfield"], .registration-form input[type="email"], .registration-form input[type="date"], .registration-form input[type="datosmascota"], .registration-form textarea').on('focus', function () {
    	$(this).removeClass('input-error');
    });
    
    // next step
    $('.registration-form .btn-next').on('click', function () {

 

        var parent_fieldset = $(this).parents('fieldset');
        var pass2 = parent_fieldset.find('input[class="form-passre form-control"]').val();
        var pass1 = parent_fieldset.find('input[class="form-pass form-control"]').val();
        var next_step = true;
        var email = parent_fieldset.find('input[class="form-email form-control"]').val();
    	emailok = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    	parent_fieldset.find('input[type="text"], input[type="password"],input[type="email"]').each(function () {

    	    if ($(this).val() == "") {
    			$(this).addClass('input-error');
    			next_step = false;
    		}

    		else if (!(email=="") && !emailok.test(email)) {
    		    parent_fieldset.find('input[class="form-email form-control"]').addClass('input-error');
    		    $("#mensajeerror").text("Ese no es un email v�lido");
    		    next_step = false;
    		}

    		else if (pass1.length<6) {

    		    parent_fieldset.find('input[class="form-pass form-control"]').addClass('input-error');
    		    $("#mensajeerror").text("La contrase�a debe tener al menos 6 car�cteres");
    		    next_step = false;
    		}

    		else if (pass1 != pass2) {
    		    parent_fieldset.find('input[class="form-passre form-control"]').addClass('input-error');
    		    $("#mensajeerror").text("Las contrase�as deben coincidir");
    		    next_step = false;
    		}

    		else {
    		    $(this).removeClass('input-error');
    		    $("#mensajeerror").text("");
    		}
    	});
    	
    	parent_fieldset.find('select').each(function () {

    	    if ($(this).val() == "") {
    	        $(this).addClass('input-error');
    	        next_step = false;
    	    }
    	    else {
    	        $(this).removeClass('input-error');
    	    }
    	});

    	parent_fieldset.find('input[type="datosmascota"], textarea, input[type="date"]').each(function () {

    	    if ($(this).val() == "") {
    	        $(this).addClass('input-error');
    	        next_step = false;
    	    }
    	    else {
    	        $(this).removeClass('input-error');
    	    }
    	});

    	if( next_step ) {
    	    parent_fieldset.fadeOut(400, function() {
    	        $(this).next().fadeIn();
    	    });
    	}
    	
    });

    
    
    // previous step
    $('.registration-form .btn-previous').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
    		$(this).prev().fadeIn();
    	});
    });
    
    // submit
    $('.registration-form').on('submit', function(e) {
    	
        $(this).find('input[type="text"], input[type="password"], input[type="email"], select,input[type="datosmascota"],input[type="date"], textarea').each(function () {
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });
    
    
});
