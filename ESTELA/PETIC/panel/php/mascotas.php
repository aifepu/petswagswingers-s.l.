<?php
require_once("PHPPaging.lib.php");
$pagina = new PHPPaging;
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
if(!isset($_SESSION['administrador']))
{
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<?php  include_once "conexion.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<header>
  <?php
    include ("headeradmin.php");
  ?>
</header>
<body>
    <div class="contenido">
      <div class="table-responsive">
        <table class="table table-hover" style="margin-left:1%">
		        <thead>
			           <tr>
				               <th>Nombre</th>
                       <th>Fecha de nacimiento</th>
				               <th>Raza</th>
                       <th>Dueño</th>
                       <th>Sexo</th>
                       <th>Estado</th>
                       <th>Descripción</th>
                       <th>Fotografía</th>
			           </tr>
		        </thead>
		        <tbody>
              <form method="post" action="eliminarmascota.php">
                <input type="text" class="form-control" name="codigo" placeholder="Introduce nombre para eliminar o modificar">
                <button type="submit" class="btn btn-danger" name="eliminar" style="width:50%;height:50%:float:right;">Eliminar mascota</button>
                <button type="submit" class="btn btn-warning" name="modificar" style="width:50%;height:50%;float:left;">Modificar mascota</button>
            </form>
                <?php
                $pagina->agregarConsulta("select * from mascota");
                $pagina->ejecutar();
                  mysql_query("SET NAMES 'utf8'");

                  while($res=$pagina->fetchResultado()){?>
                    <tr>
                    <td><?php  echo $res['nombre']; ?></td>
                    <td><?php  echo $res['fechaNacimiento']; ?></td>
                    <td><?php  echo $res['codraza']; ?></td>
                    <td><?php  echo $res['dueno']; ?></td>
                    <td><?php  echo $res['sexo']; ?></td>
                    <td><?php  echo $res['estado']; ?></td>
                    <td><?php  echo $res['descripcion']; ?></td>
                    <td><?php  echo '<img height="30%" width="60%" src="data:image;base64,'.$res['fotografia'].' ">'; ?></td>
                    </tr>
                  <?php }?>
            </tbody>
        </table>

        <?php echo 'Paginas '.$pagina->fetchNavegacion(); ?>
      </div>
    </div>


</body>
</html>
