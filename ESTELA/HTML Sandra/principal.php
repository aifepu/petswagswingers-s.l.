<?php
	include ("seguridad.php");
?>
<html>
	<head>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/jquery.bxslider.css" />

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
 		<title>Petic: Pagina de Inicio</title>
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>
			<!-- Mascotas -->
					<div id="mascotas-wrapper">
						<header>
							<h3>Mascotas cerca de ti</h3>
						</header>
							<div class="spotlights">
								<ul class="bxslider">
									<?
									include_once "php/conexion.php";
									$consulta="SELECT nombre, descripcion, fotografia FROM mascota";

									$query=mysql_query($consulta);
									if(mysql_num_rows($query)>0){
										?>
										<li>
										<?
										$cont16=0;
										$cont4=0;
										while(($fila=mysql_fetch_array($query)) and ($cont16<16)){
											$nombre=$fila["nombre"];
											$descripcion=$fila["descripcion"];
											$fotografia=$fila["fotografia"];

											if($cont4==4){
												$cont4=0;
												?>
												</li>
												<li>
												<?
											}
											if($cont4<5){
												?>
												<div class="caja-mascota" >
													<?$nombre;?>
												</div>
												<?
												$cont4++;
											}
											$cont16++;
										}
										?>
										</li>
										<?
									}else{
										echo "Error: no existen datos";
									}
									?>
								</ul>

							</div>



				</div>

            <!-- Anuncios -->
				<div id="anuncios-wrapper">
                        <header><h3>Ultimos anuncios</h3></header>
						<div class="row">
                            <div>
							<section class="widget thumbnails">
									<div class="postit">Esto es un anuncio, donde pondra un trozo del anuncio como aqui...<a href="#">Leer mas</a></div>
							</section>
                            </div>
                            <div>
							<section class="widget thumbnails">
									<div class="postit">Esto es un anuncio, donde pondra un trozo del anuncio como aqui...<a href="#">Leer mas</a></div>
							</section>
                            </div>
                            <div>
							<section class="widget thumbnails">
									<div class="postit">Esto es un anuncio, donde pondra un trozo del anuncio como aqui...<a href="#">Leer mas</a></div>
							</section>
                            </div>
                            <div>
							<section class="widget thumbnails">
									<div class="postit">Esto es un anuncio, donde pondra un trozo del anuncio como aqui...<a href="#">Leer mas</a></div>
							</section>
                            </div>
						</div>
				</div>

			<!-- Actividades -->
				<div id="actividades-wrapper">
                        <header>
								<h3>Ultimas actividades</h3>
						</header>
						<div class="row">
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Put something here</h2>
												<p>Maybe here as well I think</p>
											</header>
											<p>Phasellus quam turpis, feugiat sit amet in, hendrerit in lectus. Praesent sed semper amet bibendum tristique fringilla.</p>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured"><img src="images/pic02.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>An interesting title</h2>
												<p>This is also an interesting subtitle</p>
											</header>
											<p>Phasellus quam turpis, feugiat sit amet in, hendrerit in lectus. Praesent sed semper amet bibendum tristique fringilla.</p>
										</div>
									</section>

							</div>
							<div class="4u 12u(medium)">

								<!-- Box -->
									<section class="box feature">
										<a href="#" class="image featured"><img src="images/pic03.jpg" alt="" /></a>
										<div class="inner">
											<header>
												<h2>Oh, and finally ...</h2>
												<p>Here's another intriguing subtitle</p>
											</header>
											<p>Phasellus quam turpis, feugiat sit amet in, hendrerit in lectus. Praesent sed semper amet bibendum tristique fringilla.</p>
										</div>
									</section>

							</div>
						</div>
				</div>

				<footer>
					<?php
						include("footer.php");
					?>
				</footer>
			<script src="assets/js/jquery.bxslider.js"></script>
			<script>
			$(document).ready(function(){
			  $('.bxslider').bxSlider();
			});
			</script>
	</body>

</html>
