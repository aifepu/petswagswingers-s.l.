<?php
	include ("seguridad.php");
?>
<html>
	<head>
		<title>Petic: Editar perfil</title>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->


 		<link href="assets/bootstrap/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		<script src="assets/js/cargaDatos.js" type="text/javascript"></script>
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>


		 <form method="post" action="php/actualizarusuario.php" style="margin-left:1%">
		 
		 <?php 
		 	include_once "php/conexion.php";
		 	$usuario = $_SESSION['usuario'];
			$consulta = "SELECT u.email, u.nombre, u.nick, u.apellidos,u.codciudad,c.codigoprovincia FROM usuario u inner join ciudad c on codciudad=c.codigo where email='$usuario'";
			$resultado = mysql_query($consulta);
			$row = mysql_fetch_array($resultado);
			
			$email= $row['email'];
			$nick= $row['nick'];
			$nombre= $row['nombre'];
			$apellidos= $row['apellidos'];
			$ciudad= $row['codciudad'];
			$provincia= $row['codigoprovincia'];
		 ?>
		 
		 	<div id="publicaranuncios-wrapper">
			<div class="cabeceraanuncios"><h3>Editar perfil</h3></div>
			<div class="anuncio container" id="formulariopublicar">
				<div class="form-group">
				  <label>Email:</label>
				  <?php echo '<input id="email" type="email" class="form-control" name="email"
						 placeholder="'.$email.'">' ?>
				</div>
				<div class="form-group">
				  <label>Nombre de usuario:</label>
				  <?php echo '<input id="nick" type="text" class="form-control" name="nick"
						  placeholder="'.$nick.'">' ?>
				</div>
				<div class="form-group">
				  <label>Contraseņa:</label>
				  <input minlength="6" id="pass" maxlength="16" type="password" class="form-control" name="pass"
						 placeholder="Inserte su nueva contraseņa">
				</div>
				<div class="form-group">
				  <label>Nombre:</label>
				  <?php echo '<input id="nombre" type="text" class="form-control" name="nombre"
						 placeholder="'.$nombre.'">' ?>
				</div>
				<div class="form-group">
				  <label>Apellidos:</label>
				  <?php echo '<input id="apellidos" type="text" class="form-control" name="apellidos"
						 placeholder="'.$apellidos.'">' ?>
				</div>
				                        <div class="form-group">
				                        	<label for="form-provincia">Provincia</label>
				                        	<?php
						                        include_once "php/conexion.php";

												$consulta = "SELECT * FROM provincia";
												$resultado = mysql_query($consulta);

											    echo '<select class="registerfield" name="regpro" id="regpro" onchange="cargaCiudades(this.value); return false;">';
												  echo'<option value="">Selecciona tu nueva provincia</option>';
											    while ($row = mysql_fetch_array($resultado)) {
											    	
											                  unset($cod, $nombre);
											                  $cod = $row['codigo'];
											                  $nombre = $row['nombre']; 
											                  echo '<option value="'.$cod.'">'.$nombre.'</option>';

											}

											    echo "</select>";
											?> 
				                        </div>
										<div class="form-group">
				                        	<label for="form-ciudad">Ciudad</label>
				                        	<div id="selectciudad">
					                        	<select class="registerfield" name="regciu" id="regciu">
					                        	<option value="">Selecciona tu nueva ciudad</option>
					                        	</select>
				                        	</div>
				                        </div>
				<button type="submit" name ="enviar" class="btn btn-sample">Guardar cambios</button>
			</div>
        </form>
		<footer>
			<?php
				include("footer.php");
			?>
		</footer>
	</body>
</html>