<?php
	include ("seguridad.php");
?>
<html>
	<head>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/jquery.bxslider.css" />
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
 		<title>Petic: Pagina de Inicio</title>
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>
			<!-- Mascotas -->
					<div id="mascotas-wrapper">
						<header>
							<h3>Mascotas cerca de ti</h3>
						</header>
							<div class="spotlights">
								<ul class="bxslider">
									<?php


									include_once "php/conexion.php";
									$consulta="SELECT m.nombre nombre, m.fotografia fotografia, u.nick nick
												FROM mascota m, usuario u
												where m.dueno=u.email and m.estado='Activo' and
												u.codciudad =
													(SELECT codciudad FROM usuario WHERE email='$usuario') and
												m.dueno!='$usuario'
									 			ORDER BY rand()";
									$query=mysql_query($consulta);
									if(mysql_num_rows($query)>0){
										?>
										<li>
										<?php
										$cont16=0;
										$cont4=0;
										while(($fila=mysql_fetch_array($query)) and ($cont16<16)){
											$nombre=$fila["nombre"];
											$nick=$fila["nick"];
											$fotografia=$fila["fotografia"];

											if($cont4==4){
												$cont4=0;
												?>
												</li>
												<li>
												<?php
											}
											if($cont4<5){
												$link='"mascota.php?nombre='.$nombre.'&nick='.$nick.'"';
												?>
												<div class="caja-mascota" >
													<a href=<?=$link;?>>

														<?php echo $nombre;?>
														<img height="100%" width="100%" src="data:image;base64,<?php echo $fotografia?>">
													</a>
												</div>
												<?php
												$cont4++;
											}
											$cont16++;
										}
										?>
										</li>
										<?php
									}else{
										echo "Error: no existen datos";
									}
									?>
								</ul>

							</div>
						</div>



				</div>

            <!-- Anuncios -->
				<div id="anuncios-wrapper">
                        <header><h3>Ultimos anuncios</h3></header>
						<div class="row">
							<?php
							include_once "php/conexion.php";
							$consulta = "SELECT titulo, contenido FROM anuncio ORDER BY fecha DESC LIMIT 4";
							$resultado = mysql_query($consulta);
						    while ($row = mysql_fetch_array($resultado)) {
					                unset($titulo, $contenido);
					                $titulo = $row['titulo'];
					                $contenido2 = substr($row['contenido'], 0, 50);
					                echo '<div>';
					                echo '<section class="widget thumbnails">';
									echo '<div class="postit">'.$titulo.'<br>'.$contenido2.'...<br><a href="anuncios.php">Leer mas</a></div>';
									echo '</section>';
									echo '</div>';

							}
							?>
						</div>
				</div>

			<!-- Actividades -->
				<div id="actividades-wrapper">
                        <header>
								<h3>Ultimas actividades</h3>
						</header>
						<div class="row">
						<?php
							include_once "php/conexion.php";
							$consulta = "SELECT codigo, nombre, descripcion, imagen, fechaInicio FROM actividad ORDER BY fechaInicio DESC LIMIT 3";
							mysql_query("SET NAMES 'utf8'");
							$resultado = mysql_query($consulta);
						    while ($row = mysql_fetch_array($resultado)) {
					                unset($cod, $nombre, $descripcion, $imagen, $fechaInicio);
					                $cod = $row['codigo'];
					                $nombre = $row['nombre'];
					                $descripcion = $row['descripcion'];
					                $imagen = $row['imagen'];
					                $fechaInicio = $row['fechaInicio'];
					                $codigo = '<div class="4u 12u(medium)">
					                				<section class="box feature">
					                					<a href="actividad.php?codigo='.$cod.'" class="image featured"><img height="300em" width="100%" src="data:image;base64,'.$imagen.'"></a>
														<div class="inner">
															<header>
																<h2>'.$nombre.'</h2>
																<p>'.$fechaInicio.'</p>
															</header>
														</div>
													</section>

											</div>';
											echo $codigo;

							}
							mysql_close();
							?>

				</div>

				<footer>
					<?php
						include("footer.php");
					?>
				</footer>
			<script src="assets/js/jquery.bxslider.js"></script>
			<script>
			$(document).ready(function(){
			  $('.bxslider').bxSlider();
			});
			</script>
	</body>

</html>
