<?php
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
if(!isset($_SESSION['administrador']))
{
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php  include_once "conexion.php";?>


<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>

<header>
  <?php
    include ("headeradmin.php");
  ?>
</header>
<body>

    <div class="contenido">
      <form method="post" action="insercion_anuncio.php" style="margin-left:1%">
        <div class="form-group">
          <label for="titulo">Título del anuncio</label>
          <input type="text" class="form-control" name="titulo"
                 placeholder="Introduce el título del anuncio">
        </div>
        <div class="form-group">
          <label for="contenido">Contenido del anuncio</label>
          <input type="text" class="form-control" name="contenido"
                 placeholder="Introduce la descripción del anuncio">
        </div>
        <div class="form-group">
          <label for="autor">Nombre del autor</label>
          <input type="text" class="form-control" name="autor"
                 placeholder="admin" value="admin" readonly="readonly">
        </div>
        <div class="form-group">
         <label>Categoria animal objetivo (opcional):</label>
             <?php
             include("dropdowncatanimal.php");
           ?>
        </div>
        <div class="form-group">
         <label>Provincia objetivo (opcional):</label>
             <?php
             include("dropdownprovincias.php");
           ?>
        </div>

        <button type="submit" name ="enviar" class="btn btn-default">Enviar</button>
</form>


</body>
</html>
