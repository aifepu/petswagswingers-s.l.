DELIMITER |

-- Procedimiento para volcar las ciudades en la tabla ciudadMem

CREATE PROCEDURE ciudadCopia()
BEGIN
IF (SELECT count(*) FROM ciudadMem) < 1 THEN
INSERT INTO ciudadMem (codigo, nombre, codigoprovincia) (SELECT ciudad.codigo, ciudad.nombre, ciudad.codigoprovincia FROM ciudad);
END IF;
END
|

-- Faltaria controlar que no haya duplicados de parejas de 
-- mascotas
CREATE PROCEDURE compatibilidad_mascotas(in eldueno varchar(50), in elnombre varchar(30), in laraza smallint unsigned, in elsexo enum('Macho','Hembra'), in laciudad smallint unsigned)

BEGIN
	DECLARE done BOOL DEFAULT 0;
	DECLARE auxdueno varchar(50);
	DECLARE auxnombre varchar(30);
	DECLARE auxciudad  smallint unsigned;

-- Cursos que recoge solo el nombre y due�o de las mascotas 
-- que cumplen que no son dle mismo due�o, ni sexo y tienen
-- misma raza.
DECLARE cur1 CURSOR FOR SELECT dueno, nombre FROM mascota WHERE dueno!=eldueno AND sexo!=elsexo AND codraza=laraza;
DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done=1;
	
	OPEN cur1;
	REPEAT
		FETCH cur1 INTO auxdueno,auxnombre;
		IF NOT done THEN
			-- Seleccionamos la ciudad de la mascota que se 				-- esta recogiendo en este paso del cursor
			SELECT codciudad INTO auxciudad FROM usuario, 					mascota WHERE auxdueno=dueno AND 						auxnombre=mascota.nombre AND email=dueno;
			
			CASE
				when(auxciudad=laciudad) THEN INSERT IGNORE 				INTO matching VALUES(elnombre,eldueno, 					auxnombre,auxdueno, NOW());
				else begin end;
		  	END CASE;
		END IF;
	UNTIL done END REPEAT;
	CLOSE cur1;

END |

CREATE PROCEDURE insertar_cita(in lamascota1 varchar(30), in eldueno1 varchar(50), in lamascota2 varchar(30), in eldueno2 varchar(50), in lafecha datetime, in lavaloracion decimal)

BEGIN

DECLARE mediavaloracion, valoracionOld decimal;
DECLARE cuantos int;


SELECT count(*) INTO cuantos FROM cita WHERE nombreMascota1 = lamascota1 AND dueno1 = eldueno1 AND nombreMascota2 = lamascota2 AND dueno2 = eldueno2 AND fecha=lafecha;

SELECT valoracion INTO valoracionOld FROM cita WHERE nombreMascota1 = lamascota1 AND dueno1 = eldueno1 AND nombreMascota2 = lamascota2 AND dueno2 = eldueno2 AND fecha=lafecha;

SET mediavaloracion = (valoracionOld+lavaloracion)/2;

IF cuantos<1 THEN INSERT INTO cita VALUES(lamascota1,eldueno1, lamascota2,eldueno2,lafecha,lavaloracion);

ELSE UPDATE  cita SET valoracion = mediavaloracion WHERE nombreMascota1 = lamascota1 AND dueno1 = eldueno1 AND nombreMascota2 = lamascota2 AND dueno2 = eldueno2 AND fecha=lafecha;
END IF;

END |
