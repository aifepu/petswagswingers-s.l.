<!DOCTYPE HTML>
<link rel="stylesheet" href="assets/css/bootstrap.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<html>
	<head>
        <meta charset="UTF-8"/>
		<title>Petic: Pagina de Inicio</title>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<script src="assets/js/busqueda.js"></script>
	</head>
	<body>
						<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse"
												data-target=".navbar-ex1-collapse">
									<span class="sr-only">Desplegar navegación</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<a href="principal.php" style="margin-right:20px;"><img src="images/brand.png"alt=""/></a>
							</div>

							<!-- Agrupar los enlaces de navegación, los formularios y cualquier
									 otro elemento que se pueda ocultar al minimizar la barra -->
							<div class="collapse navbar-collapse navbar-ex1-collapse">
								<ul class="nav navbar-nav">
									<li><a style="height:54px;" href="principal.php">Inicio</a></li>
									<li><a style="height:54px;" href="mascotas.php">Mascotas</a></li>
									<li><a style="height:54px;" href="anuncios.php">Anuncios</a></li>
									<li><a style="height:54px;" href="actividades.php">Actividades</a></li>
									<?php  $nick = $_SESSION['nick']; ?>
									<li><a style="height:54px;" href="perfil.php?nick=<?php  echo $nick?>">Mi perfil</a></li>
									<li><a style="height:54px;" href="contacto.php">Contacto</a></li>
									<li style="height:50px;margin-left:20px;"><a href="tusactividades.php"><img src="images/actividad.png"/></a></li>
									<?php 
									
										include_once "php/conexion.php";
										$usuario = $_SESSION['usuario'];
										
										$con = "SELECT * from amistad where usuario2='$usuario' and estado='Pendiente'";
										$resultado = mysql_query($con);
										$amistad = 'nada';
										if($row = mysql_fetch_array($resultado))
										{
											$amistad = 'pendiente';
										}
										
									?>
									<li style="height:50px;margin-left:20px;display:<?php  if($amistad=='pendiente'){echo '';}else{echo 'none';}?>"><a href="solicitudesdeamistad.php"><img src="images/amistadnotificacion.gif"/></a></li>
									<li style="height:50px;margin-left:20px;display:<?php  if($amistad=='nada'){echo '';}else{echo 'none';}?>"><a href="solicitudesdeamistad.php"><img src="images/amistad.png"/></a></li>
								</ul>


								<form class="navbar-form navbar-right" role="search" action="busqueda.php">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Buscar" name="campo-busqueda" id="campo-busqueda">
									</div>
									<button type="submit" class="btn btn-default" style="margin-right:10px;"><i class="fa fa-search"></i></button>

									<a href="logout.php" class="btn btn-default" style="margin-right:10px;" role="button">Cerrar sesión</a>
								</form>
								</div>
							</nav>

							<div class="logotipo" style="border-bottom-width:2px;border-bottom-color: #ac474e;border-bottom-style: solid;">
								<div class="row" style="margin:20px 0;">
                	<div class="col-md-4" >
                  	<a href="principal.php"><img src="images/cabecera.png" alt=""/></a>
                	</div>
								</div>
							</div>
