<?php 
	include ("seguridad.php");
?>
<html>
	<head>
		<title>Petic: Solicitudes de amistad</title>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->


 		<link href="assets/bootstrap/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		
		
	</head>
	<header>
		<?php 
			include("head.php");
		?>
	</header>
	<body>
		<div id="solciitudes-wrapper">
			<div class="row"><h3>Solicitudes de amistad</h3></div>
					<?php include("php/mostrarsolicitudesamistad.php"); ?>
					</br>
	
			</div>
	
		</div>
	
		<footer>
			<?php 
				include("footer.php");
			?>
		</footer>
	</body>

</html>