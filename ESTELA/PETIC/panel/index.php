<html>
<link rel="stylesheet" type="text/css" href="css/login.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<head>
  <title>ADMINISTRACIÓN PETIC</title>
</head>
<body>
  <div id = "login">
    <form method = "post" action="php/comprobar_admin.php">
      <h2><span class="fontawesome-lock"></span>Acceso administración</h2>
      <fieldset>
        <?php 
            if(isset($_GET["error"])){
              echo "<span>Error en la autenticación</span>";
            }
        ?>
        <p><label for="usuario">Usuario administrador:</label></p>
        <input type="text" name="usuario" value="user" maxlength="15" onBlur="if(this.value=='')this.value='user'" onFocus="if(this.value=='user')this.value=''"><br><br>

        <p><label for="clave">Contraseña administrador:</label></p>
        <input type = "password" name="clave" value="password" size="15" onBlur="if(this.value=='')this.value='password'" onFocus="if(this.value=='password')this.value=''"><br><br>

        <p><input type="submit" name="submit" value="Entrar"></p>
      </form>
    </fieldset>
  </div>
</body>
</html>
