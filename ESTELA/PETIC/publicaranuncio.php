<?php 
	include ("seguridad.php");
?>
<html>
	<head>
		<title>Petic: Publicar anuncio</title>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->


 		<link href="assets/bootstrap/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	</head>
	<header>
		<?php 
			include("head.php");
		?>
	</header>
	<body>


		 <form method="post" action="php/insertar_anuncio.php" style="margin-left:1%">

		 	<div id="publicaranuncios-wrapper">
			<div class="cabeceraanuncios"><h3>Publicar un anuncio</h3></div>
			<div class="anuncio container" id="formulariopublicar">
				<div class="form-group">
				  <label>Título:</label>
				  <input required maxlength="50" type="text" class="form-control" name="titulo"
						 placeholder="Introduce el título de tu anuncio">
				</div>
				<div class="form-group">
				  <label>Contenido:</label>
				  <textarea required class="form-control" name="contenido" placeholder="Introduce el contenido de tu anuncio"></textarea>
				</div>

				<div class="form-group">
				 <label>Categoria animal objetivo (opcional):</label>
				     <?php 
						 include("php/dropdowncatanimal.php");
					 ?>
				</div>
				<div class="form-group">
				 <label>Provincia objetivo (opcional):</label>
				     <?php 
						 include("php/dropdownprovincias.php");
					 ?>
				</div>
				<button type="submit" name ="enviar" class="btn btn-sample">Publicar</button>
			</div>
        </form>
		<footer>
			<?php 
				include("footer.php");
			?>
		</footer>
	</body>
</html>
