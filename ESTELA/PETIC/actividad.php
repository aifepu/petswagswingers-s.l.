<!-- Para controlar el error de cabeceras de php-->

<?php
	ob_start();
	include ("seguridad.php");
?>
<html>
	<head>
    <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" href="assets/css/jquery.bxslider.css" />
		<link rel="stylesheet" href="assets/css/actividades.css" />

 		<title>Petic: Actividad</title>
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>
		<?php
			include_once "php/conexion.php";
			$codigo=$_GET["codigo"];
			$consulta = "SELECT a.nombre,descripcion,a.codigo,imagen,plazasocupadasmascota,plazasocupadaspersona,plazastotalespersona,plazastotalesmascota,precio,
			 											fechaInicio, fechaFin, codciudad,c.nombre city from actividad a,ciudad c
														where a.codigo = '$codigo' and a.codciudad = c.codigo;";

			$resultado = mysql_query($consulta);
		    while ($row = mysql_fetch_array($resultado)) {
					//unset($nombre, $descripcion, $imagen, );
		      $nombre = $row['nombre'];
					$descripcion = $row['descripcion'];
					$imagen = $row['imagen'];
					$plazastotalespersona = $row['plazastotalespersona'];
					$plazastotalesmascota = $row['plazastotalesmascota'];
					$plazasocupadaspersona = $row['plazasocupadaspersona'];
					$plazasocupadasmascota = $row['plazasocupadasmascota'];
					$precio = $row['precio'];
					$fechaInicio = $row['fechaInicio'];
					$fechaFin = $row['fechaFin'];
					$ciudad = $row['city'];
			}

		?>
		<div id="actividades-wrapper">
			<div class="anuncio container">
				<div class="info-actividad">
					<h3><?=$nombre;?></h3>
					<hr />
					<div class="cajaimagen">
						<img height="50%" width="50%" src="data:image;base64,<?=$imagen?>">
					</div>

					<hr />
					<table>
						<td>
							<p><h3>Precio: <?=$precio;?> €</h3></p>
							<p><h3>Ciudad: <?=$ciudad;?></h3></p>
							<p>Inicio de la inscripción: <?=$fechaInicio;?></p>
							<p>Fin de la inscripción: <?=$fechaFin;?></p>
						</td>
						<td>
							<form method="post" action="">
								<label name ="codigo" value =<?=$codigo;?>></label>

								<?php
									$cod = $_GET["codigo"];
									$usuario = $_SESSION['usuario'];
									$consulta = "SELECT usuario1,actividad1 from reserva where usuario1 = '$usuario' and actividad1 = '$cod';";
									$res = mysql_query($consulta);

									$con = "SELECT plazaslibrespersona($cod) as plazas;";
									$resultado = mysql_query($con);
									$row = mysql_fetch_array($resultado);
									$maxper = $row['plazas'];

									$con2 = "SELECT plazaslibresmascota($cod) as plazas;";
									$resultado2 = mysql_query($con2);
									$row2 = mysql_fetch_array($resultado2);
									$maxmas = $row2['plazas'];

									if (mysql_num_rows($res) == 0) {
										if ($maxper == 0 || $maxmas == 0) {
											echo '<h2>NO HAY PLAZAS DISPONIBLES</h2>';
										}
										else {
											echo '<p>Las plazas libres para personas son: '.$maxper.' de '.$plazastotalespersona.' totales</p>';
											echo '<p>Las plazas libres para mascotas son: '.$maxmas.' de '.$plazastotalesmascota.' totales</p>';
											echo '<input type="number" class="form-control" min="1" max="'.$maxper.'" name="reserva-personas" placeholder="¿Cuántas personas asistirán?"></input><br>';
											echo '<input type="number" class="form-control" min="1" max="'.$maxmas.'" name="reserva-mascotas" placeholder="¿Cuántas mascotas os acompañarán?"></input><br>';
											echo '<button type="submit" class="btn btn-default" name="enviar" style="width:90%;height:90%;">¡Apuntarse a esta actividad!</button>';
										}
									}
									else {
										echo '<p>Las plazas libres para personas son: '.$maxper.' de '.$plazastotalespersona.' totales</p>';
										echo '<p>Las plazas libres para mascotas son: '.$maxmas.' de '.$plazastotalesmascota.' totales</p>';
										echo '<h4> Si ya estás registrado, puedes modificar tu reserva... </h4>';
										echo '<input type="number" class="form-control" value="" min="1" max="'.$maxper.'" name="mod-personas" placeholder="Nuevo número de personas que asistirán"></input><br>';
										echo '<input type="number" class="form-control" value="" min="1" max="'.$maxmas.'" name="mod-mascotas" placeholder="Nuevas mascotas que os acompañarán"></input><br>';
										echo '<button type="submit" class="btn btn-warning" name="modificar" style="width:90%;height:90%;margin-bottom:10px;">Modificar mi reserva</button>';
										echo '<button type="submit" onclick = "mostrarBaja();" class="btn btn-danger" name="baja" style="width:90%;height:90%;">No podré asistir...</button>';
									}
								?>


							</form>

						</td>
					</table>

			</div>

		</div>
		</div>
		<?php
		include_once "php/conexion.php";
		if (isset($_POST['enviar'])){
			if (isset($_POST['reserva-personas']) && (isset($_POST['reserva-mascotas']))) {
				$cod = $_GET["codigo"];
				$personas = $_POST['reserva-personas'];
				$mascotas = $_POST['reserva-mascotas'];
				$usuario = $_SESSION['usuario'];

				$consulta = "INSERT INTO reserva (usuario1,actividad1,plazasreservadaspersona,plazasreservadasmascota)
												VALUES ('$usuario','$cod','$personas','$mascotas');";
				$resultado = mysql_query($consulta);
				header("location: actividades.php");
			}

		}
		?>
		<?php
			include_once "php/conexion.php";
			if (isset($_POST['baja'])){
					$cod = $_GET["codigo"];
					$usuario = $_SESSION['usuario'];
					$consulta = "DELETE FROM reserva where usuario1 = '$usuario' and actividad1 = '$cod';";
					$res = mysql_query($consulta);
						header("location: actividades.php");
				}
		?>
		<?php
			include_once "php/conexion.php";
			if (isset($_POST['modificar'])){
					$cod = $_GET["codigo"];
					$personas = $_POST['mod-personas'];
					$mascotas = $_POST['mod-mascotas'];
					$usuario = $_SESSION['usuario'];


					if($personas==""){

						$consulta = "UPDATE reserva SET  plazasreservadasmascota = '$mascotas' where usuario1 = '$usuario' and actividad1 = '$cod';";
						$res = mysql_query($consulta);
					}
					else if($mascotas==""){

						$consulta = "UPDATE reserva SET plazasreservadaspersona= '$personas' where usuario1 = '$usuario'and actividad1 = '$cod';";
						$res = mysql_query($consulta);
					}
					else{

						$consulta = "UPDATE reserva SET plazasreservadaspersona= '$personas', plazasreservadasmascota = '$mascotas' where usuario1 = '$usuario' and actividad1 = '$cod';";
						$res = mysql_query($consulta);
					}

					header("location: actividades.php");
				}
		?>
		<script>
			function mostrarBaja() {
				alert("Te has dado de baja en esta actividad... ¡Te echaremos de menos!");
			}
		</script>
<footer>
	<?php
		include("footer.php");
	?>
</footer>
	</body>
</html>
<!-- Para controlar el error de cabeceras de php-->
<?php
ob_end_flush();
?>
