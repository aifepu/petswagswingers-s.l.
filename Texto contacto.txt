Petic nace con la vocaci�n de mejorar y extender los lazos entre todos aquellos que pertenezcan a la comunidad animal, ya sean due�os o mascotas. Un proyecto pionero donde personas de todo el territorio espa�ol pueden relacionarse con todo aquel que comparta su vida con un animal afin al suyo, organizando quedadas, compartiendo informaci�n, realizando y resolviendo dudas... En resumen, est�s ante una forma �nica de encontrar tu manada :)

�Qu� hacemos?


Si por algo se caracteriza Petic es por su alto grado de usabilidad, permitiendo al usuario en unos pocos clicks encontrar aquellos animales afines a su mascota, lugar de residencia u otras preferencias personales. El contacto entre ellos es muy sencillo e intuitivo, facilitando as� la comunicaci�n.

Adem�s, nuestra ayuda para que encuentres tu manada no se queda ah�, ya que la web dispone de un sistema de anuncios donde cualquier usuario puede publicar mensajes segmentados en funci�n de los posibles interesados, facilitando as� el intercambio de ideas, productos o cualquier informaci�n interesante para nosotros y nuestros mejores amigos. Por �ltimo, desde la administraci�n proponemos actividades de forma peri�dica, a las que se podr� sumar cualquier usuario de la p�gina.

Como no, estamos abiertos a cualquier sugerencia o comentario para mejorar nuestro sistema, ya que somos conscientes de que siempre se puede mejorar, as� que, si crees que puedes ayudarnos, por favor env�anos un mensaje en el formulario que observas a tu derecha.

�Bienvenidx a tu manada!

El equipo de Petic.