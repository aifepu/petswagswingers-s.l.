function cargaCiudades(codprov){
	var parametros = {
		"codprov" : codprov
	};
	$.ajax({
    	data:  parametros,
        url:   'php/dropdownciudades.php',
        type:  'post',
        success:  function (response) {
            $("#selectciudad").html(response);
        }
    });
}

function cargaRazas(regcatanimal){
	var parametros = {
		"regcatanimal" : regcatanimal
	};
	$.ajax({
    	data:  parametros,
        url:   'php/dropdownraza.php',
        type:  'post',
        success:  function (response) {
            $("#selectraza").html(response);
        }
    });
}