<?php
//creamos la sesion
session_start();
//validamos si se ha hecho o no el inicio de sesion correctamente
if(!isset($_SESSION['administrador']))
{
  header('Location: ../index.php');
}
?>
<!DOCTYPE html>

<html lang="es">
<link rel="stylesheet" href="../css/bootstrap.css" />
<link rel="stylesheet" href="../css/estilos.css" />
<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<?php  include_once "conexion.php";?>

<head>
<title>Administración Petic</title>
<meta charset="utf-8" />
</head>

<header>
  <?php
    include ("headeradmin.php");
  ?>
</header>
<body>

    <div class="contenido">
      <form method="post" action="insercion_actividad.php" enctype="multipart/form-data" style="margin-left:1%">
        <div class="form-group">
          <label for="plazastotalespersona">Plazas para personas</label>
          <input type="number" class="form-control" name="plazastotalespersona"
                 placeholder="Introduce cantidad de plazas para personas">
        </div>
        <div class="form-group">
          <label for="plazastotalesmascotas">Plazas para mascotas</label>
          <input type="number" class="form-control" name="plazastotalesmascotas"
                 placeholder="Introduce cantidad de plazas para mascotas">
        </div>
        <div class="form-group">
          <label for="precio">Precio de la actividad</label>
          <input type="number" class="form-control" name="precio"
                 placeholder="Introduce el precio de la actividad">
        </div>
        <div class="form-group">
          <label for="fechaInicio">Fecha de inicio</label>
          <input type="date" class="form-control" name="fechaInicio"
                 placeholder="Introduce la fecha de inicio de inscripción">
        </div>
        <div class="form-group">
          <label for="fechaFin">Fecha fin de inscripción</label>
          <input type="date" class="form-control" name="fechaFin"
                 placeholder="Introduce la fecha de fin de inscripción">
        </div>
        <div class="form-group">
          <label for="nombre">Nombre de la actividad</label>
          <input type="text" class="form-control" name="nombre"
                 placeholder="Introduce el nombre de la actividad">
        </div>
        <div class="form-group">
          <label for="descripcion">Descripción de la actividad</label>
          <input type="text" class="form-control" name="descripcion"
                 placeholder="Introduce la descripción de la actividad">
        </div>
        <div class="form-group">
          <label class="sr-only" for="form-ciudad">Ciudad</label>
          <?php
            include("dropdownciudades.php");
          ?>
          </div>
        </div>



        <div class="form-group">
          <label for="archivo">Adjuntar un archivo</label>
          <input type="file" name="imagen">
          <p class="help-block">Ejemplo de texto de ayuda.</p>
        </div>
<button type="submit" name ="enviar" class="btn btn-default">Enviar</button>
</form>


</body>
</html>
