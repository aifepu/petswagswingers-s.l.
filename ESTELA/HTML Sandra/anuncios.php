<?php
	include ("seguridad.php");
?>
<html>
	<head>
		<title>Petic: Anuncios</title>
        <meta charset="UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />

		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->


 		<link href="assets/bootstrap/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="assets/jquery/jquery-1.11.3.js"></script>
		<script src="assets/bootstrap/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

 		<script type ="text/javascript">

		var paginador;
		var totalPaginas
		var itemsPorPagina = 10;
		var numerosPorPagina = 3;

		function creaPaginador(totalItems)
		{
			paginador = $(".pagination");

			totalPaginas = Math.ceil(totalItems/itemsPorPagina);

			$('<li><a href="#" class="first_link"><</a></li>').appendTo(paginador);
			$('<li><a href="#" class="prev_link">«</a></li>').appendTo(paginador);

			var pag = 0;
			while(totalPaginas > pag)
			{
				$('<li><a href="#" class="page_link">'+(pag+1)+'</a></li>').appendTo(paginador);
				pag++;
			}


			if(numerosPorPagina > 1)
			{
				$(".page_link").hide();
				$(".page_link").slice(0,numerosPorPagina).show();
			}

			$('<li><a href="#" class="next_link">»</a></li>').appendTo(paginador);
			$('<li><a href="#" class="last_link">></a></li>').appendTo(paginador);

			paginador.find(".page_link:first").addClass("active");
			paginador.find(".page_link:first").parents("li").addClass("active");

			paginador.find(".prev_link").hide();

			paginador.find("li .page_link").click(function()
			{
				var irpagina =$(this).html().valueOf()-1;
				cargaPagina(irpagina);
				return false;
			});

			paginador.find("li .first_link").click(function()
			{
				var irpagina =0;
				cargaPagina(irpagina);
				return false;
			});

			paginador.find("li .prev_link").click(function()
			{
				var irpagina =parseInt(paginador.data("pag")) -1;
				cargaPagina(irpagina);
				return false;
			});

			paginador.find("li .next_link").click(function()
			{
				var irpagina =parseInt(paginador.data("pag")) +1;
				cargaPagina(irpagina);
				return false;
			});

			paginador.find("li .last_link").click(function()
			{
				var irpagina =totalPaginas -1;
				cargaPagina(irpagina);
				return false;
			});

			cargaPagina(0);




		}

		function cargaPagina(pagina)
		{
			var desde = pagina * itemsPorPagina;

			$.ajax({
				data:{"param1":"dame","limit":itemsPorPagina,"offset":desde},
				type:"GET",
				dataType:"json",
				url:"php/conexionanuncios.php"
			}).done(function(data,textStatus,jqXHR){

				var lista = data.lista;

				$("#anuncios").html("");

				$.each(lista, function(ind, elem){

					var div = document.getElementById('anuncios');

					div.innerHTML = div.innerHTML +
					'<div class="anuncio container">'+
					'<div class="row">'+
					'<h4>'+elem.titulo+'</h4>'+
					'</div>'+
					'</br>'+
					'<div class="anunciocontenido">'+elem.contenido+'</div>'+
					'<div class="row">'+
					'<a href="perfil.php?nick='+elem.autor+'">'+elem.autor+'</a><label class ="normal">'+elem.fecha+'</label>'+
					'</div>'+
					'</div>'+
					'</div>'+'</br>';


				});


			}).fail(function(jqXHR,textStatus,textError){
				alert("Error al realizar la peticion dame".textError);

			});

			if(pagina >= 1)
			{
				paginador.find(".prev_link").show();

			}
			else
			{
				paginador.find(".prev_link").hide();
			}


			if(pagina <(totalPaginas- numerosPorPagina))
			{
				paginador.find(".next_link").show();
			}else
			{
				paginador.find(".next_link").hide();
			}

			paginador.data("pag",pagina);

			if(numerosPorPagina>1)
			{
				$(".page_link").hide();
				if(pagina < (totalPaginas- numerosPorPagina))
				{
					$(".page_link").slice(pagina,numerosPorPagina + pagina).show();
				}
				else{
					if(totalPaginas > numerosPorPagina)
						$(".page_link").slice(totalPaginas- numerosPorPagina).show();
					else
						$(".page_link").slice(0).show();

				}
			}

			paginador.children().removeClass("active");
			paginador.children().eq(pagina+2).addClass("active");


		}


		$(function()
		{

			$.ajax({

				data:{"param1":"cuantos"},
				type:"GET",
				dataType:"json",
				url:"php/conexionanuncios.php"
			}).done(function(data,textStatus,jqXHR){
				var total = data.total;

				creaPaginador(total);


			}).fail(function(jqXHR,textStatus,textError){
				alert("Error al realizar la peticion cuantos".textError);

			});



		});



		</script>
	</head>
	<header>
		<?php
			include("head.php");
		?>
	</header>
	<body>
				<div id="anuncios-wrapper">

				<div class="cabeceraanuncios"><a href="publicaranuncio.php" class="btn btn-sample btn-lg" role="button">Publicar anuncio</a></div>

				<div class="cabeceraanuncios"><h3>Todos los anuncios</h3></div>

				<div id="anuncios" class="anuncios">
				</div>

                   <div class "col-md-12 text-center">
                   		<ul class="pagination" id="paginador"></ul>
                   </div>

				</div>

				<footer>
					<?php
						include("footer.php");
					?>
				</footer>
	</body>

</html>
